<div class="row">
	<div class="span6 offset3 thumbnail">
		<img src="img/logo_v1.png" alt=""/>
	</div>
</div>
<div class="row">
	<div class="span6 offset3" id="login-box">
		<form class="form-horizontal" method="POST">
			<input type="hidden" name="q" value="login">
			<fieldset>
				<legend>Bejelentkezés</legend>
				<div class="control-group">
					<label class="control-label" for="emailInput">E-mail</label>
					<div class="controls">
						<input class="input-xlarge focused" id="emailInput" type="text" value="" name="emailInput">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="passwInput">Jelszó</label>
					<div class="controls">
						<input class="input-xlarge focused" id="passwInput" type="password" value="" name="passwInput">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Belépés</button>
					<a href="registration" class="btn btn-primary">Regisztráció</a>
					<a href="forget" class="btn btn-primary">Jelszó emlékeztető</a>
				</div>
			</fieldset>
		</form>
	</div>
</div>