<div class="row">
	<div class="span9">
		<div class="row">
			<div class="span9">
				<h2>Ajánlott képek</h2>
			</div>
			<div class="span9">
				<ul class="thumbnails">
					<?php
					foreach($recommendedImages as $image) {
						?>
					<li class="span3">
						<a href="image/<?php echo $image['ID']?>" class="thumbnail">
							<img src="show_image.php?id=<?php echo $image['ID']; ?>&thumb=true" alt="">
						</a>
					</li>			
						<?php
					}
					?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="span9">
				<h2>Véletlen képek</h2>
			</div>
			<div class="span9">
				<ul class="thumbnails">
					<?php
					foreach($randomImages as $image) {
						?>
					<li class="span3 meret">
						<a href="image/<?php echo $image['ID']?>" class="thumbnail">
							<img src="show_image.php?id=<?php echo $image['ID']; ?>&thumb=true" alt="">
						</a>
					</li>			
						<?php
					}
					?>
				</ul>
			</div>
		</div>		
	</div>
	<div class="span3">
		<table class="table">
			<thead>
				<tr>
					<th>Kategóriák</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach($categories as $category) {
					?>
					<tr>
						<td><a href="category/<?php echo $category['ID']; ?>"><?php echo utf8_encode($category['NAME']); ?></a></td>
					</tr>		
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>