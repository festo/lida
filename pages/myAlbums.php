<div class="row">
	<div class="span12">
		<h2>Albumok</h2>
		<ul class="thumbnails" id="myAlbums">
			<li class="span3">
				<div class="thumbnail">
					<div id="new-album" class="well">
						<form class="" method="POST">
							<input type="hidden" name="q" value="new-album">
							<p>Az album címe</p>
							<input type="text" class="span2" id="inputSuccess" name="albumInput"><br />
							<button type="submit" class="btn btn-primary">Létrehozás</button>
						</form>
					</div>
					<div class="caption">
						<h5>Új album létrehozása</h5>	
					</div>
				</div>
			</li>
			<?php
			try {
				foreach($albumIds as $albumId) {
				//	var_dump($albumId);
				//	exit;
					$album = new Album($albumId['ID']);
					?>
					<li class="span3">
						<div class="thumbnail">
							<a href="myImages/<?php echo $albumId['ID']; ?>">
								<img src="show_image.php?id=<?php echo firstImage($albumId['ID']); ?>&thumb=true" alt="">
							</a>
							<div class="caption">
								<h5><?php echo $album->getTitle(); ?></h5>	
							</div>
						</div>
					</li>
					<?php
				}
			} catch(NotExistingAlbumIdException $e) {
				?>
				<div class="alert alert-error">
						<a class="close" data-dismiss="alert">×</a>
						A művelet során hiba történt. <br /> Nem létező album azonosító.
					</div>
				<?php
			}
			?>
		</ul>
	</div>
</div>