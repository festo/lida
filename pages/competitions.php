<div class="row">
	<div class="span8">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Kiíró</th>
					<th>Cím</th>
					<th>Kezdő dátum</th>
					<th>Lejárat</th>
					<th>Győztes</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach($comps as $comp) {
					if ($comp['WINNER'] == -1){
						$winnerEmail = '';
					} else {
						$db->query("SELECT EMAIL FROM USERS WHERE ID=". $comp['WINNER']);
						$winnerEmail=$db->fetchAll();
						if ($db->numRows() != 1){
							$_Error[] = "Nem létezik a győztes felhasználó!";
							exit;
						}
						$winnerEmail = $winnerEmail[0]['EMAIL'];
					}
					?>
					<tr>
						<td><?php echo $comp['ID']; ?></td>
						<td><a href="users/<?php echo $comp['ID']; ?>"><?php echo $comp['EMAIL']; ?></a></td>
						<td><a href="competitionData/<?php echo $comp['ID']; ?>"><?php echo $comp['TITLE']; ?></a></td>
						<td><?php echo $comp['START_DATE']; ?></td>
						<td><?php echo $comp['END_DATE']; ?></td>
						<td><a href="users/<?php echo $comp['WINNER']; ?>"><?php echo $winnerEmail; ?></a></td>
					</tr>		
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<div class="span4 right-side">
		<?php
		include_once "pages/rightSide.php";
		?>
	</div>
</div>