<div class="row">
	<div class="span7 offset2">
		<h2>Kép szerkesztése</h2>
	</div>
	<div class="span1">
		<a href="myImages/<?php echo $imageData['ALBUM']; ?>" class="btn btn-primary">Vissza</a>
	</div>
</div>
<div class="row">
	<div class="span3 offset2">
		<ul class="thumbnails">
			<li class="span3">
				<a href="image/<?php echo $URL[1]; ?>" class="thumbnail">
					<img src="show_image.php?id=<?php echo $URL[1]; ?>&thumb=true" alt="">
				</a>
			</li>
		</ul>
	</div>
	<div class="span5">
		<form class="well form-horizontal" method="POST">
			<input type="hidden" name="q" value="edit-image">
			<fieldset>
				<div class="control-group">
					<label class="control-label" for="inputSuccess">Cím</label>
					<div class="controls">
						<input type="text" class="input-xlarge" name="title" value="<?php echo $imageData['TITLE']; ?>" id="inputSuccess">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputSuccess">Leírás</label>
					<div class="controls">
						<textarea class="input-xlarge" id="textarea" rows="3" name="text"><?php echo $imageData['TEXT']; ?></textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="multiSelect">Kategóriák</label>
					<div class="controls">
						<select multiple="multiple" name="cat[]" id="multiSelect">
							<?php
							foreach($categories as $cat) {

								echo '<option value="'.$cat['ID'].'"';
								if(in_array($cat['ID'], (array)$selectedCategories)) {
									echo ' selected="selected"';
								}
								echo '>'.utf8_encode($cat['NAME']).'</option>';
							}
							?>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"></label>
					<div class="controls">
						<button type="submit" class="btn btn-primary">Mentés</button>
						<a href="deleteImage/<?php echo $URL[1]; ?>" class="btn btn-danger" onclick="javascript:return confirm('Biztos, hogy törölni akarja a képet?')"><i class="icon-trash icon-white"></i> Kép törlése</a>
					</div>
				</div>				
			</fieldset>
		</form>
	</div>
</div>
<div class="row">
	<div class="span8 offset2">
		<hr />
		
			<?php
				$db = new db();
				$db->query("SELECT COMMENTS.ID AS COMM_ID, USER_ID, NAME, RESPONSE, TEXT, TO_CHAR(when, 'YYYY.MM.dd HH24:MI') AS COMMTIME FROM COMMENTS, USERS WHERE image_id=". $URL[1] ." AND users.id = comments.user_id ORDER BY WHEN DESC");
				$result=$db->fetchAll();
				foreach($result as $row){
					echo "<div class='row comment'>
		<div class='span5 well comment-box'>
				<div class='header'>
					<a href='users/". $row['USER_ID'] ."'>". $row['NAME'] ."</a> | ". $row['COMMTIME'] ."
				</div>
				";
					if ($row['RESPONSE'] != NULL) echo "<b>@". $row['NAME'] ."</b> ";
					echo $row['TEXT'] ."
			</div>";
			
					echo "<div class='span2 action'>
				<a class='btn btn-danger' href='deleteComment/". $row['COMM_ID'] ."' onclick='javascript:return confirm(\"Biztos, hogy törölni akarja a hozzászólást?\")'><i class='icon-trash icon-white'></i> Törlés</a>
			</div>
		</div>";
				}
			?>
		
	</div>
</div>