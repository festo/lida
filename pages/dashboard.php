<div class="row">
	<div class="span12">
		<div class="hero-unit">
			<h1>Helló, ez itt a Lida!</h1>
			<p>Ez a Lida nevű képmegosztó portál, melyet direkt, csak nektek, fotósoknak készítettünk el. Van benne minden hasznos funkció, mint például követés, meg a fotópályázat. Stb ...</p>
			<p>Sajnos bökdösni nem lehet, de helyette kommentelhettek a képekhez, és értékelhetitek azokat.</p>
			<p>Készítették:
				<ul>
					<li>Bódis Attila</li>
					<li>Farkas József</li>
					<li>Munkácsy Gergely</li>
				</ul>
			</p>
		</div>
	</div>
</div>
<div class="row">
	<div class="span12">
		<h2>Követések</h2>
		<ul class="thumbnails">
			<?php
				foreach($followerImages as $image) {
					?>
			<li class="span3">
				<a href="image/<?php echo $image['ID']?>" class="thumbnail">
					<img src="show_image.php?id=<?php echo $image['ID']; ?>&thumb=true" alt="">
				</a>
			</li>		
					<?php
				}
			?>
		</ul>
	</div>
</div>