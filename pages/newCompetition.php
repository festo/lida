<div class="row">
	<div class="span6 offset3 well">
		<form class="form-horizontal" method="POST">
			<input type="hidden" name="q" value="newCompetition">
			<fieldset>
				<div class="control-group">
					<label class="control-label" for="inputTitle">Cím</label>
					<div class="controls">
						<input class="input-xlarge" type="text" id="inputTitle" name="inputTitle" value="">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputText">Leírás</label>
					<div class="controls">
						<textarea class="input-xlarge" id="inputText" name="inputText" rows="3"></textarea>
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEnd">Lejárat</label>
					<div class="controls">
						<input type="text" class="span2" value="<?php echo date("Y-m-d"); ?>" data-date-format="yyyy-mm-dd" data-date="<?php echo date("Y-m-d"); ?>" id="inputEnd" name="inputEnd">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Létrehozás</button>
				</div>
			</fieldset>
		</form>
	</div>
</div>


<script type="text/javascript">
	$('#inputEnd').datepicker({
		weekStart: 1
	})
		.on('changeDate', function(ev){
			$('#inputEnd').val($('#inputEnd').data('date'));
			$('#inputEnd').datepicker('hide');
		});	
</script>