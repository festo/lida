<div class="row">
	<div class="span8 offset2">
		<div class="thumbnail">
			<img src="show_image.php?id=<?php echo $URL[1]; ?>" alt="" />
			<!-- <img src="http://placehold.it/800x555" alt=""> -->
			<div class="caption">
				<h5><?php echo $imageData['TITLE']; ?></h5>
				<p><?php echo $imageData['TEXT']; ?></p>
				<h6>Feltöltötte: <a href="users/<?php echo $imageData['USER_ID']; ?>"><?php echo $imageData['NAME']; ?></a></h6>
				<h6>Album: <a href="<?php echo (($imageData['USER_ID'] == $_User->getUserId()) ? "myImages/" : "images/" ) . $imageData['ALBUM_ID']; ?>"><?php echo $imageData['ALBUM_TITLE']; ?></a></h6>
				<h6>Kategóriák: 
				<?php
					$db->query("SELECT categories.id AS cat_id, name FROM categories, images_and_categories WHERE image_id = :img_id AND categories.id = category_id", array(':img_id' => $imageData['ID']));
					$cats = $db->fetchAll();
					foreach ($cats as $categ){
						echo '<a href="category/'. $categ['CAT_ID'] .'">'. utf8_encode($categ['NAME']) .'</a> ';
					}
				?>
				</h6>
				<p><div>
					<b>Értékelés</b>
					<div id="star"></div>
				</div></p>
				<div id="imageId" class="hidden"><?php echo $URL['1']; ?></div>
				<script type="text/javascript">
				$(document).ready(function(){
					$('#star').raty({
						<?php if($isVote != 0 || $imageData['USER_ID'] == $_User->getUserId()) { echo 'readOnly : true,'; } ?>						
						score    : <?php echo $imageRate; ?>,
						noRatedMsg : 'Már szavaztál!',
						click: function(score, evt) {
							imageVote(score, $('#imageId').html());
						}
					});
				});
				</script>
				<a href="show_image.php?id=<?php echo $URL[1]; ?>" class="btn btn-success">Letölt</a>
				<?php
					if ($imageData['USER_ID'] == $_User->getUserId()){
						echo '<a href="editImage/'. $imageData['ID'] .'" class="btn btn-primary">Szerkeszt</a>';
					}
				?>
			</div>
		</div>
	</div>
	<div class="span8 offset2">
		<hr />
	</div>
	<div class="span4 offset4" id="comment">
		<h4>Szólj hozzá!</h4>
		<form class="form-horizontal" method="POST">
			<input name="q" value="comment" type="hidden">
			<fieldset>
				<textarea class="input-xlarge" id="inputComment" name="inputComment" rows="3"></textarea>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Hozzászólok</button>
				</div>
			</fieldset>
			<input id="inputResponse" name="inputResponse" value="" type="hidden">
		</form>
	</div>
	<div class="span8 offset2">
		<form id="commentVote" class="form-horizontal" method="POST">
			<input name="q" value="commentVote" type="hidden">
			<input id="inputCommID" name="inputCommID" value="" type="hidden">
			<input id="inputCommVote" name="inputCommVote" value="" type="hidden">
		</form>
		
		<?php
		$db = new db();
		$db->query("SELECT COMMENTS.ID as ID, USER_ID, NAME, RESPONSE, TEXT, TO_CHAR(when, 'YYYY.MM.dd HH24:MI') AS COMMTIME FROM COMMENTS, USERS WHERE image_id=". $URL[1] ." AND users.id = comments.user_id ORDER BY WHEN DESC");
		$result=$db->fetchAll();
		foreach($result as $row){
			echo '<div class="well comment-box">
			<div class="header">
			<a href="users/'. $row['USER_ID'] .'">'. $row['NAME'] .'</a> | '. $row['COMMTIME'] .'
			<a id="resp_'. $row['ID'] .'" href="javascript:void(0)" onclick="setResp('.$row['ID'].');" class="btn">Válasz</a>
			';
			$db->query("SELECT * FROM comment_votes WHERE USER_ID = ". $_User->getuserId() ." AND comment_id = ". $row['ID']);
			$vote_res=$db->fetchAll();
			if ($db->numRows() != 1){
				echo '<a onclick="commentVote('. $row['ID'] .', 1);" class="btn btn-primary">+1</a>
				<a onclick="commentVote('. $row['ID'] .', -1);" class="btn btn-danger">-1</a>';
			} else if ($vote_res[0]['VALUE'] == 1) {
				echo '<a onclick="commentVote('. $row['ID'] .', 1);" class="btn btn-primary disabled">+1</a>
				<a onclick="commentVote('. $row['ID'] .', -1);" class="btn disabled">-1</a>';
			} else {
				echo '<a onclick="commentVote('. $row['ID'] .', 1);" class="btn disabled">+1</a>
				<a onclick="commentVote('. $row['ID'] .', -1);" class="btn btn-danger disabled">-1</a>';
			}
			
			$db->query("SELECT SUM(value) AS avg_value FROM comment_votes WHERE comment_id = ". $row['ID']);
			$avgVote_res = $db->fetchAll();
			if ($db->numRows()==1){
				echo ' <strong>'. $avgVote_res[0]['AVG_VALUE'] .'</strong>';
			}
			echo '</div>
			';
			if($row['RESPONSE'] != NULL) {
				$db->query("select users.name from comments, users where users.id = comments.user_id and comments.id = ".$row['RESPONSE']);
				$res = $db->fetchAll();

			}
			if ($row['RESPONSE'] != NULL) echo "<b>@". $res[0]['NAME'] ."</b> ";
			echo $row['TEXT'] ."
			</div>";
		}
		?>
		
	</div>
</div>