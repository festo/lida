<div class="row">
	<div class="span6">
		<h3>Új kép feltöltése</h3>
		<div class="drag-drop-area focus">
			<div class="drag-drop-inside">
				<p class="drag-drop-info">Válassza ki a fájlt!</p>
				<form class="form-vertical" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="type" value="upload">
					<input type="hidden" name="albumId" value="<?php echo $album['ID']; ?>">
					<input class="input-file" id="fileInput" type="file" name="file"><br />
					<button type="submit" class="btn btn-primary">Feltöltés</button>
				</form>
			</div>
		</div>
	</div>
	<div class="span6">
		<h3>Album szerkesztése</h3>
		<div class="drag-drop-area focus" id="edit-album">
			<form class="form-inline" method="POST">
				<input type="hidden" name="type" value="edit">
				<input type="hidden" name="albumId" value="<?php echo $album['ID']; ?>">
				<input type="text" class="input-large" name="title" value="<?php echo $album['TITLE']; ?>">
				<button type="submit" class="btn btn-primary">Átnevezés</button>
			</form>
			<a href="deleteAlbum/<?php echo $album['ID']; ?>" class="btn btn-danger" onclick="javascript:return confirm('Biztos, hogy törölni akarja az albumot?')">Album törlése</a>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="span12">
		<h2>Képek</h2>
		<ul class="thumbnails">
			<?php
			foreach((array)$images as $image) {
				$id = array_values($image);
				$id = $id[0];
				?>
				<li class="span3">
					<a href="editImage/<?php echo $id; ?>" class="thumbnail">
						<img src="show_image.php?id=<?php echo $id; ?>&thumb=true" alt="">
					</a>
				</li>					
				<?php
			}
			?>
		</ul>
	</div>
</div>