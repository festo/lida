<?php
if(!$profile) {
?>
<div class="row">
	<div class="span8">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>E-mail cím</th>
					<th>Név</th>
					<th>Követés</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($users as $user) {
						$userObject = new User($user['ID']);
						$userData = $userObject->getUserData(array('email', 'name'));
						$follow = '<a href="users/follow/'.$user['ID'].'" class="btn">Követés</a>';
						if($_User->isFollow($user['ID'])) {
							$follow = '<a href="users/unfollow/'.$user['ID'].'" class="btn btn-primary">Követve</a>';
						}
						?>
				<tr>
					<td><?php echo $user['ID']; ?></td>
					<td><a href="users/<?php echo $user['ID']; ?>"><?php echo $userData['email']; ?></a></td>
					<td><?php echo $userData['name']; ?></td>
					<td><?php echo $follow; ?></td>
				</tr>		
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
	<div class="span4 right-side">
		<?php
			include_once "pages/rightSide.php";
		?>
	</div>
</div>
<?php
} else {
?>

<div class="row">
	<div class="span6 offset3 well">
		<form class="form-horizontal">
			<fieldset>
				<div class="control-group">
					<label class="control-label disabled" for="inputEmail">E-mail cím</label>
					<div class="controls">
						<input class="input-xlarge" type="text" id="inputEmail" name="inputEmail" disabled="" value="<?php echo $userData['email']; ?>">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputName">Név</label>
					<div class="controls">
						<input class="input-xlarge disabled" type="text" id="inputName" name="inputName" disabled="" value="<?php echo $userData['name']; ?>">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPhone">Telefon</label>
					<div class="controls">
						<input class="input-xlarge disabled" type="text" id="inputPhone" name="inputPhone" disabled="" value="<?php echo $userData['phone']; ?>">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputIntroduction">Bemutatkozás</label>
					<div class="controls">
						<textarea class="input-xlarge disabled" id="inputIntroduction" name="inputIntroduction" rows="3" disabled=""><?php echo $userData['introduction']; ?></textarea>
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPhone">Érték</label>
					<div class="controls">
						<input class="input-xlarge disabled" type="text" id="inputPhone" name="inputPhone" disabled="" value="<?php echo $_User->getRate($_User->getUserId()); ?>">
						<span class="help-inline"></span>
					</div>
				</div>				
			</fieldset>
		</form>
	</div>
</div>		

<div class="row">
	<div class="span12">
		<h2>Albumok</h2>
		<ul class="thumbnails" id="myAlbums">
			<?php
			try {
				foreach($albumIds as $albumId) {
				//	var_dump($albumId);
				//	exit;
					$album = new Album($albumId['ID']);
					?>
					<li class="span3">
						<div class="thumbnail">
							<a href="images/<?php echo $albumId['ID']; ?>">
								<img src="show_image.php?id=<?php echo firstImage($albumId['ID']); ?>&thumb=true" alt="">
							</a>
							<div class="caption">
								<h5><?php echo $album->getTitle(); ?></h5>	
							</div>
						</div>
					</li>
					<?php
				}
			} catch(NotExistingAlbumIdException $e) {
				?>
				<div class="alert alert-error">
						<a class="close" data-dismiss="alert">×</a>
						A művelet során hiba történt. <br /> Nem létező album azonosító.
					</div>
				<?php
			}
			?>
		</ul>
	</div>
</div>

<?php
}
?>