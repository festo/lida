<div class="navbar">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="/Lida/">Lida</a>
			<div class="nav-collapse">
				<ul class="nav">
					<li id="home"><a href="dashboard">Kezdőlap</a></li>
					<li><a href="browse">Böngészés</a></li>
					<li><a href="myAlbums">Képeim</a></li>
					<li><a href="users">Felhasználók</a></li>
					<li><a href="competitions">Pályázatok</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Beállítások <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="userSettings">Adatok módosítása</a></li>
							<li><a href="follow">Követések</a></li>
							<li class="divider"></li>
							<li class="nav-header">Fotópályázatok</li>
							<li><a href="newCompetition">Új fotópályázat</a></li>
						</ul>
					</li>
				</ul>
				<ul class="nav pull-right">
					<li class="divider-vertical"></li>
					<li><a href="logout">Kijelentkezés</a></li>
				</ul>
			</div><!-- /.nav-collapse -->
		</div>
	</div><!-- /navbar-inner -->
</div>