<div class="row">
	<div class="span6 offset3 thumbnail">
		<img src="img/logo_v1.png" alt=""/>
	</div>
</div>
<div class="row">
	<div class="span4 offset4">
		<div id="succesRegistration">
			<p class="bigger">Sikeres regisztráció!</p>
			<p>A megadott adatokkal most már bejelentkezhet!</p>
			<p><a href="login" class="btn btn-primary">Bejelentkezés</a></p>
		</div>
	</div>
</div>