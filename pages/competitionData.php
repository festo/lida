<div class="row">
	<div class="span6 offset3 well">
		<form class="form-horizontal" method="POST">
			<fieldset>
				<div class="control-group">
					<label class="control-label" for="inputTitle">Cím</label>
					<div class="controls">
						<input disabled="" class="input-xlarge disabled" type="text" id="inputTitle" name="inputTitle" value="<?php echo $competition['TITLE']; ?>">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputText">Leírás</label>
					<div class="controls">
						<textarea disabled="" class="input-xlarge disabled" id="inputText" name="inputText" rows="3"><?php echo $competition['TEXT']; ?></textarea>
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEnd">Kezdet</label>
					<div class="controls">
						<input disabled="" type="text" class="span2 disabled" value="<?php echo $competition['START_DATE']; ?>"  id="inputEnd" name="inputEnd">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEnd">Lejárat</label>
					<div class="controls">
						<input disabled="" type="text" class="span2 disabled" value="<?php echo $competition['END_DATE']; ?>"  id="inputEnd" name="inputEnd">
						<span class="help-inline"></span>
					</div>
				</div>				
			</fieldset>
		</form>
	</div>
</div>

<div class="row">
	<div class="span6 offset3 well">
		<?php 
		if(!$voteEnd) {
		?>
		<div class="sendNewImage"><h4>Saját kép benevezése</h4></div>
		<form class="form-horizontal" method="POST">
			<input type="hidden" name="q" value="sendImage">
			<fieldset>
				<div class="control-group">
					<label class="control-label" for="multiSelect">A képeid</label>
					<div class="controls">
						<select multiple="multiple" id="multiSelect" name="img[]" style="width: 300px; height: 150px">
							<?php
							foreach($userImages as $image) {
								echo '<option value="'.$image['ID'].'">'.$image['ALBUM'].' - '.$image['IMAGE'].'</option>';
							}
							?>
						</select>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Benevezés</button>
				</div>				
			</fieldset>
		</form>
		<?php } else { 
			$db->query("select count(image_id) as cou, image_id as id from competition_vote where competition_id = :competition_id group by image_id order by cou DESC", array(":competition_id" => $URL[1]));
			$eredm = $db->fetchAll();

			?>
		<div class="sendNewImage"><h4>Nyertes kép</h4></div>
		<div class="sendNewImage"><a href="image/<?php echo $eredm[0]['ID']; ?>"><img src="show_image.php?id=<?php echo $eredm[0]['ID']; ?>&thumb=true" alt=""></a></div>
		<?php } ?>
	</div>
</div>

<div class="row">
	<div class="span12">
		<h3>Benevezett képek</h3>
		<form id="imageVote" name="imageVote" class="form-horizontal" method="POST">
			<input name="q" value="imageVote" type="hidden">
			<input id="inputImageID" name="inputImageID" value="" type="hidden">
		</form>
		<ul class="thumbnails" id="myAlbums">
			<?php
			foreach($images as $img) {
				// szavazott-e-mar
				$db->query("SELECT * FROM competition_vote where user_id = :user_id and image_id = :image_id and competition_id = :comp_id", array(":image_id" => $img['ID'], ":comp_id" => $URL[1], ":user_id" => $_User->getUserId()));
				$db->fetchAll();
				$szavaz = "";
				if($db->numRows() != 0) {
					$szavaz = " disabled";
				}


				// hanyan
				$db->query("SELECT * FROM competition_vote where image_id = :image_id and competition_id = :comp_id", array(":image_id" => $img['ID'], ":comp_id" => $URL[1]));
				$data = $db->fetchAll();
				$count = $db->numRows();

				$button = "";
				if(!$voteEnd) {
					$button = "<a ";
					if($szavaz == "") {
						$button .= 'onclick="ImageVote('.$img['ID'].');"';
					}
					$button .= 'class="btn '.$szavaz.'">Szavazás</a>';
				}

				?>	
				<li class="span3">
					<div class="thumbnail">
						<a href="image/<?php echo $img['ID']; ?>">
							<img src="show_image.php?id=<?php echo $img['ID']; ?>&thumb=true" alt="">
						</a>
						<div class="caption">
							<?php echo $button; ?> <a href="" class="btn disabled"><?php echo $count; ?></a>	
						</div>
					</div>
				</li>			
				<?php
			}
			?>
		</ul>
	</div>
</div>