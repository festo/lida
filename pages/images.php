<div class="row">
	<div class="span12">
		<h2>Képek</h2>
		<ul class="thumbnails">
			<?php
			foreach((array)$images as $image) {
				$id = array_values($image);
				$id = $id[0];
				?>
				<li class="span3">
					<a href="image/<?php echo $id; ?>" class="thumbnail">
						<img src="show_image.php?id=<?php echo $id; ?>&thumb=true" alt="">
					</a>
				</li>					
				<?php
			}
			?>
		</ul>
	</div>
</div>