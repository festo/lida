<div class="row">
	<div class="span8">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>E-mail cím</th>
					<th>Név</th>
					<th>Követés</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($users as $user) {
						$userObject = new User($user['ID']);
						$userData = $userObject->getUserData(array('email', 'name'));
						$follow = '<a href="follow/unfollow/'.$user['ID'].'" class="btn btn-primary">Követve</a>';
						?>
				<tr>
					<td><?php echo $user['ID']; ?></td>
					<td><a href="users/<?php echo $user['ID']; ?>"><?php echo $userData['email']; ?></a></td>
					<td><?php echo $userData['name']; ?></td>
					<td><?php echo $follow; ?></td>
				</tr>		
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
	<div class="span4 right-side">
		<?php
			include_once "pages/rightSide.php";
		?>
	</div>
</div>