<div class="row">
	<div class="span8">
		<form class="form-horizontal" method="POST">
			<input name="q" value="setDeffaultData" type="hidden">
			<fieldset>
				<legend>Adatok módosítása</legend>
				<div class="control-group">
					<label class="control-label" for="inputEmail">E-mail cím</label>
					<div class="controls">
						<input class="input-xlarge disabled" type="text" id="inputEmail" name="inputEmail" placeholder="<?php echo $userData['email']; ?>" disabled="">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputName">Név</label>
					<div class="controls">
						<input class="input-xlarge" type="text" id="inputName" name="inputName" value="<?php echo $userData['name']; ?>">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPhone">Telefon</label>
					<div class="controls">
						<input class="input-xlarge" type="text" id="inputPhone" name="inputPhone" value="<?php echo $userData['phone']; ?>">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputIntroduction">Bemutatkozás</label>
					<div class="controls">
						<textarea class="input-xlarge" id="inputIntroduction" name="inputIntroduction" rows="3"><?php echo $userData['introduction']; ?></textarea>
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Mentés</button>
					<a href="dashboard" class="btn">Mégse</a>
				</div>
			</fieldset>
		</form>
		<form class="form-horizontal" method="POST">
			<input name="q" value="changeEmail" type="hidden">
			<fieldset>
				<legend>E-mail cím módosítása</legend>
				<div class="control-group">
					<label class="control-label" for="inputEmail">E-mail cím</label>
					<div class="controls">
						<input class="input-xlarge disabled" type="text" id="inputEmail" name="inputEmail">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail2">E-mail cím mégegyszer</label>
					<div class="controls">
						<input class="input-xlarge" type="text" id="inputEmail2" name="inputEmail2">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPassw">Jelszó</label>
					<div class="controls">
						<input class="input-xlarge" type="password" id="inputPassw" name="inputPassw">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Mentés</button>
					<a href="dashboard" class="btn">Mégse</a>
				</div>
			</fieldset>
		</form>
		<form class="form-horizontal" method="POST">
			<input name="q" value="changePass" type="hidden">
			<fieldset>
				<legend>Jelszó módosítása</legend>
				<div class="control-group">
					<label class="control-label" for="inputEmail">Új jelszó</label>
					<div class="controls">
						<input class="input-xlarge disabled" type="password" id="inputNewPassw" name="inputNewPassw">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail2">Új jelszó mégegyszer</label>
					<div class="controls">
						<input class="input-xlarge" type="password" id="inputNewPassw2" name="inputNewPassw2">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPassw">Régi jelszó</label>
					<div class="controls">
						<input class="input-xlarge" type="password" id="inputOldPassw" name="inputOldPassw">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Mentés</button>
					<a href="dashboard" class="btn">Mégse</a>
				</div>
			</fieldset>
		</form>		
	</div>
	<div class="span4 right-side">
		<?php
			include_once "pages/rightSide.php";
		?>
	</div>
</div>
</div>