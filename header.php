<?php
session_start();
ob_start(); // Ez azert kell, hogy elobb dolgozza fel a php utasitasokat és csak utanna renderelje a fajlt. Ez azert jo, mert ha valahol lesz egy header() hivas akkor nem akad ki....

// A megfelelo osztalyok betoltese
include_once "classes/database/OracleWrapper.class.php";
include_once "classes/user/User.class.php";
include_once "classes/album/Album.class.php";

// ha meg nem allitottunk be alapertelmezett erteket
if(!isset($_SESSION['is_logged'])) {
	$_SESSION['is_logged'] = false;
	$_SESSION['user_id'] = null;
}

$_Error = array();

// Az urlet darabokra bontjuk es beallitjuk ha kell
$URL = array();
if(isset($_GET['rewrite'])) {
	$URL = explode("/", $_GET["rewrite"]); 
} else {
	$URL[0] = null;
}

try {
	// Letrehozunk egy User-t a sessionnek
	if($_SESSION['is_logged'] && ($_SESSION['user_id'] > 0)) {
		$_User = new User($_SESSION['user_id']);
	} else {
		$_User = new User();
	}	
} catch(NotExistsUserIdException $e) {
	$_Error[] = "Nemlétező user ID";
	session_destroy(); // Ilyenkor jelentkezzen be ujra
}

if(!$_SESSION['is_logged']) {
	// Ha nincs bejelentkezve
	switch ($URL[0]) {
		default:
		case 'login':
		include_once "controll/login.php";	
		break;

		case 'registration':
		include_once "controll/registration.php";	
		break;
		
		case 'forget':
		include_once "controll/forget.php";	
		break;
	}

} else {
	if(!isset($URL[0])) {
		// ha nincs beallitva semmi akokr alapertelmezett betoltese
		include_once "controll/dashboard.php";
	} else if(isset($URL[0]) && file_exists("controll/".$URL[0].".php")) {
		// ha van, es letezik a controll fajl, betoltjuk
		include_once "controll/".$URL[0].".php";
	}
}

?>