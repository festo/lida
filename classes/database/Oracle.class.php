<?php 

/** 
 * Az osztaly ezen a rendszeren alapszik:
 * http://www.phpclasses.org/package/1262-PHP-Oracle-database-access-wrapper-class.html
 * 
 * Kigyomlaltam belole ami nem kell illetve ami nem mukodik.
 * @author Festo
 */ 
class Oracle 
{ 
	var $_Sid = ''; // session id 
	var $_User = ''; // username 
	var $_Password = ''; // password 
	var $_conn = null; 
	var $_stmt = null; 
	var $_row = 0; 
	var $_rowcount = 0; 

	var $Error = ''; 
	var $Halt_On_Error = 'no'; // yes|no 

	var $Debug = 0; // 1 to set debug mode 

	var $Record = array(); // array for current record in result 

	var $_commitMode = OCI_COMMIT_ON_SUCCESS; // OCI_DEFAULT 

	var $_queriesStack = array(); 
	var $_lastQuery; 

	var $_blindSQL = null;
	var $_blindArray = array();


	/** 
	 * Class constructor. 
	 * Sets connection parameters (in array), if given. 
	 */ 
	function Oracle($arrp = null)  
	{ 
		if ($arrp != null)  
		{ 
			$this->_Sid = $arrp[0]; 
			$this->_User = $arrp[1]; 
			$this->_Password = $arrp[2]; 
		} 
	} 

	/**
	 * Visszaadja az eredmeny egy sorat asszociativ tomben.
	 * Iteralni kell a teljes eredmeny megtekintesehez
	 */
	function fetchArray() 
	{
		return oci_fetch_assoc($this->_stmt);
	}

	/**
	 * Visszaadja az eredmeny egy sorat. 
	 */
	function fetchRow()
	{
		return oci_fetch_row($this->_stmt);
	}

	/**
	 * Debug mod beallitasa 
	 */
	function setDebug()
	{
		$this->Debug = 1;
	}

	/**
	 * Az eredemny osszes soraval visszateregy tomben
	 */
	function fetchAll() 
	{
		$this->_rowcount = oci_fetch_all($this->_stmt, $ret, null, null, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
		return $ret;
	}

	/**
	 * Visszaadja a lekerdezes sorainak a szamat
	 * Elobb a fetchAll fgv-t meg kell hivni!
	 */
	function numRows() {
		return $this->_rowcount;
	}

	/** 
	 * This method is for compability reasons with Mysql.lib.php. 
	 * Does not return the last inserted id, but always 0 (zero). 
	 * 
	 * Here is how you can get last inserted id in Oracle: 
	 * (you have to have sequence named USERS_SEQ for table USERS)  
	 * <code> 
	 *    $sql = "INSERT INTO USERS(FIRSTNAME, LASTNAME, USERNAME[, ...]) VALUES ( 'blaa', 'blaa', 'blaa'[, ...])"; 
	 *    if ( !$dbc->query($sql) ) 
	 *        return 0; // query failed 
	 * 
	 *    $sql = 'SELECT USERS_SEQ.CURRVAL AS NEW_ID FROM DUAL'; 
	 *    if ( $dbc->query($sql) && $dbc->next_record() ) 
	 *        return $dbc->f('NEW_ID'); // return squence's current value (last inserted id for table USERS) 
	 * </code> 
	 * @return 0 
	 */ 
	function last_insert_id() 
	{ 
		return 0; 
	} 

	/** 
	 * Connects to database. 
	 * Returns TRUE if connection succeeded, FALSE otherwise. 
	 * @return boolean 
	 */ 
	function connect($Sid = '', $User = '', $Password = '')  
	{ 
		if ('' == $Sid) $Sid = $this->_Sid; 
		if ('' == $User) $User = $this->_User; 
		if ('' == $Password) $Password = $this->_Password; 

		$this->Error = null; 
		if ($this->_conn != null)  
		{ 
			$this->disconnect(); 
		} 

		$this->_conn = oci_connect($User, $Password, $Sid); 
		if (!$this->_conn)  
		{ 
			$this->Error = OCIError($this->_conn); 
			$this->halt('connect(' . $Sid . ',' . $User . ',\$Password) failed.'); 
			return FALSE; 
		} 
		return TRUE; 
	} 

	/** 
	 * Disconnects from database. 
	 * Returns FALSE if no connection present ($this->_conn is NULL), TRUE otherwise. 
	 * @return boolean 
	 */ 
	function disconnect() 
	{ 
		$this->Error = null; 
		if ($this->_conn == null)  
			return FALSE; 
		oci_close($this->_conn); 
		return TRUE; 
	} 

	/** 
	 * Executes given query. 
	 * Returns TRUE if query executed successfully, FALSE otherwise. 
	 * @return boolean 
	 */ 
	function query($sql,$bind = null) 
	{ 
		$this->Error = null; 

		if ($this->_conn == null) $this->connect(); 
		if (!$this->_conn) return FALSE; 

		if ( $this->Debug ) printf("Debug: query = %s<br>\n", $sql); 

		$this->_stmt = oci_parse($this->_conn, $sql); 

		if (!$this->_stmt)  
		{ 
			$this->Error = oci_error($this->_stmt); 
			return FALSE; 
		} 

		if(!is_null($bind)) {
			foreach($bind as $key => $value) {
				oci_bind_by_name($this->_stmt, $key, $bind[$key]);
			}
		}

		$this->_blindSQL = $sql;
		$this->_blindArray = $bind;

		if ( !oci_execute($this->_stmt,$this->_commitMode) )  
		{             
			$this->Error = oci_error($this->_stmt); 

			if ( $this->Debug ) echo "DEBUG: $sql<BR>"; 
			$this->halt("Invalid SQL: ".$sql); 

			return FALSE; 
		} 
		$this->_row = 0; 
		$this->registerQuery($sql);
		return TRUE; 
	} 

	function blindDebug() {
		var_dump($this->_blindSQL);
		var_dump($this->_blindArray);
	}

	/** 
	 * Seeks to row nr given in parameter $r. 
	 */ 
	function seek($r) 
	{ 
		$this->_row = $r; 
	} 

	/** 
	 * Returns given field's ($name) value 
	 */ 
	function f($name)  
	{ 
		return $this->Record[$name]; 
	} 

	/** 
	 * Halts (kills Oracle session). 
	 * Shows error message and number. 
	 * @seealso get_error_string() 
	 */ 
	function halt($msg)  
	{ 
		if ( $this->Halt_On_Error == 'no' ) 
		{ 
			// trigger_error($this->Error, E_USER_ERROR); 
			trigger_error($this->get_error_string(), E_USER_ERROR); 
			return; 
		} 
		die($this->Error); 
	} 

	/** 
	 * Returns Oracle error (Array) as string. 
	 * @return array 
	 */ 
	function get_error_string() 
	{         
		if ( is_string($this->Error) ) 
			return $this->Error; 
		else if ( !is_array($this->Error) )     
			return null; 

		if ( isset($this->Error['code']) ) 
			$err = 'code: ' . $this->Error['code'] . "\n "; 
		if ( isset($this->Error['message']) ) 
			$err .= 'message: ' . $this->Error['message'] . "\n "; 
		if ( isset($this->Error['offset']) ) 
			$err .= 'offset: ' . $this->Error['offset'] . "\n "; 
		if ( isset($this->Error['sqltext']) ) 
			$err .= 'sqltext: ' . $this->Error['sqltext']; 

		return $err; 
	} 

	/** 
	* Start a transaction. 
	* @author sina.salek.ws 
	*/ 
	function startTransaction()  
	{ 
		$this->_commitMode=OCI_DEFAULT; 
	} 

	/** 
	* Apply changes to database. 
	* @author sina.salek.ws 
	*/ 
	function commit()  
	{ 
		OCICommit($this->_conn); 
	} 

	/** 
	* Rollback changes within current open transaction. 
	* @author sina.salek.ws 
	*/ 
	function rollback()  
	{ 
		OCIRollback($this->_conn); 
	} 

	/** 
	* Add query to queries stack. 
	* @author sina.salek.ws 
	*/ 
	function registerQuery($query)  
	{ 
		$this->_lastQuery=$query; 
		$this->_ueriesStack[]=$query; 
	} 

	/** 
	* Get the last executed query. 
	* @author sina.salek.ws 
	*/ 
	function getLastQuery()  
	{ 
		return $this->_lastQuery; 
	} 
} 
?>