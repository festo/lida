<?php
// 2012.04.28 - festo - Atirtam benne nehany lenyegesebb dolgot.
// Forras: http://net.tutsplus.com/tutorials/php/image-resizing-made-easy-with-php/

Class resize
{
	private $image;
	private $width;
	private $height;
	private $imageResized;
	private $type;

	function __construct($source, $type)
	{

		$this->image = imagecreatefromstring($source);
		$this->type = $type;

		$this->width  = imagesx($this->image);
		$this->height = imagesy($this->image);
	}

	public function resizeImage($newWidth, $newHeight)
	{
		$optionArray = $this->getDimensions($newWidth, $newHeight);

		$optimalWidth  = $optionArray['optimalWidth'];
		$optimalHeight = $optionArray['optimalHeight'];

		$this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
		imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);
	}

	private function getDimensions($newWidth, $newHeight) {
		if($newHeight > $this->height || $newWidth > $this->width) {
			return array('optimalWidth' => $this->width, 'optimalHeight' => $this->height);
		}

		$rateHeight = $newHeight / $this->height;
		$rateWidth = $newWidth / $this->width;
		$rate = $rateWidth;
		if($rateHeight < $rateWidth) {
			$rate = $rateHeight;
		}
		$size = array('optimalWidth' => (int)($rate*$this->width), 'optimalHeight' => (int)($rate*$this->height));
		
		return $size;
	}

	public function getSource() {

		$src = null;
		ob_start();
		switch ($this->type) {
			case 'image/gif':
			imagegif($this->imageResized);
			break;

			case 'image/jpeg':
			imagejpeg($this->imageResized);
			break;

			case 'image/png':
			imagepng($this->imageResized);
			break; 			
		}

		$src = ob_get_contents();
		ob_end_clean();
		return $src;
	}

	function __destuctor() {
		imagedestroy($this->imageResized);
		imagedestroy($this->image);
	}

}
?>
