<?php

include_once "UserExceptions.php";

/**
 * Figyelni kell, hogy a tagfuggvenyeket csak akkor szabad meghivni, ha be van jelentkezve... 
 */
class User {

	var $_id	= null; // User ID

	/**
	 * Alapertelmezett konstruktor
	 * Ha mar bejelentkezett a felhasznalo akkor kap egy parametert, a User ID-t, aminek segítsegevel beallitja az adatokat
	 */
	function __construct($ID = null) {
		if($ID != null) {
			$db = new db();
			$db->query("SELECT id FROM USERS WHERE id = ".$ID);
			$result = $db->fetchAll();

			if($db->numRows() != 1)
				throw new NotExistsUserIdException;

			$this->_id = $ID;
		}
	}

	/**
	 * Bejelentkezest lekezelo fgv.
	 * Ha sikeres akkor igazat ad vissza, ha nem pedig hamisat.
	 * Ha valami nem megfelelo akkor kivetelt dob.
	 * Bellaitja az $this->_id erteket!
	 * @param $email bejelentkezeshez hasznalt e-mail cim
	 * @param $passw bejelentkezeshez hasznalt jelszo
	 * @return  boolean
	 */
	function login($email,$password) {
		// Az email megfelelo?
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
			throw new NotValidEmailException();
		} 

		// A password megfelelo?
		if(!$password || strlen($password = trim($password)) == 0) {
			throw new NotValidPasswordException();
		}

		$password = md5($password);

		$db = new db();
		$db->query("SELECT id FROM USERS WHERE email = '$email' and passw = '$password'");

		$result = $db->fetchAll();

		if($db->numRows() != 1) {
			return false;
		} else {
			$this->_id = $result[0]['ID'];
			return true;
		}
	}

	/**
	 * Uj felhazsnalo regisztralasa
	 * Az e-mail cimnek egyedinek kell lennie
	 * A jelszot md5()-kodolassal taroljuk.
	 * Ha valami nem ok akkor dobjon kivetelt
	 * Ha sikeres a regisztracio akkor true, kulonben false
	 * @param $email A hasznalni kivant e-mail cim
	 * @param $passw A hasznalni kivant jelszo
	 * @return  boolena 
	 */
	function newUser($email,$password)	{
		// Az email megfelelo?
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
			throw new NotValidEmailException();
		} 

		// A password megfelelo?
		if(!$password || strlen($password = trim($password)) == 0) {
			throw new NotValidPasswordException();
		}

		$password = md5($password);

		$db = new db();	

		// A mail cim egyedi?
		$db->query("SELECT id FROM USERS WHERE email = '$email'");
		$db->fetchAll();
		if($db->numRows() != 0) {
			throw new ExistingEmailException();	
		}

		if($db->query("INSERT INTO USERS (EMAIL, PASSW) VALUES ('$email', '$password')")) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * A megadott e-mail cimhez tartozo jelszot felulirjuk egy 8 karakter hosszu uj random str-el.
	 * Majd kuldunk a felhasznalonak errol egy e-mailt.
	 * Sikeres muvelet eseten true kulonben false
	 * @param $email A resetelendo felhasznalo
	 * @return boolean
	 */
	function resetPassw($email)	{
		// Az email megfelelo?
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
			throw new NotValidEmailException();
		}
		
		$db = new db();
		
		$db->query("SELECT id FROM users WHERE email='$email'");
		$result=$db->fetchAll();
		if ($db->numRows() == 0){
			throw new NotExistsUserIdException();
		}
		
		$new_pass = $this->generateRandStr();
		$msg="Az Ön jelszavát a kérésére megváltoztattuk.
			
Bejelentkezési adatai:

	Email cím: $email
	Jelszó: $new_pass

Üdvözlettel,
Lida - Staff";
		
		// TODO: A vegen esetleg odafigyelhetunk arra, hogy ha nem megy el az e-mail akkor irjuk ki kepernyore az uj jelszot vagy valami.
		mail($email,"Jelszó változtatás", $msg);
		
		$change_pass = $db->query("UPDATE users SET passw = :new_passw WHERE id=".$result[0]['ID'], array(':new_passw' => md5($new_pass)));
		
		return $change_pass; 
	}

	/**
	 * A felhasznalo adatait adja vissza egy asszociativ tomben.
	 * Ha nem ures parametterrel hivjuk meg akkor az adott ertekeket adja csak vissza.
	 * Ha nem ad meg parametert akkor az osszes adattal terjen vissza.
	 * Ha olyan parametret kap ami nem letezik dobjon kivetelt
	 * @param $param A kivant adatok egy tomben. pl: getUserData(array('email', 'phone'));
	 * @return  array pl: aray('email' => 'test@test.com', 'phone' => '06301234567');
	 */
	function getUserData($param = null) {
		$deffault = array('email', 'name', 'introduction', 'phone');

		if(!is_null($param)) {
			$query = array_intersect((array)$param, $deffault); // Vesszuk a ket Array metszetet
			if($param != $query) { // ha nem egyezik meg a ket array akkor ez egyik parameter amit megadtunk nem letezik
				throw new NotExistingParameterException();
			}
		} else {
			$query = $deffault;
		}

		// Osszeallitjuk a lekerdezest
		$SQL = "Select ";
		$comma = "";
		foreach($query as $item) {
			$SQL .= $comma . " " . $item;
			$comma = ",";
		}

		$SQL .= " FROM USERS WHERE ID = ".$this->_id;

		$db = new db();
		$db->query($SQL);
		$result = $db->fetchAll();

		return self::arrayToLower($result[0]);

	}

	/**
	 * Beallitja a felhasznalo adatait a megfeleo parameterekkel.
	 * Ha nem megfelelo parametert akar beallitani akkor dobjon kivetelt
	 * Ha sikeres akkor true-val terjen vissza, kulonben false
	 * @param $param egy asszociativ array pl.: aray('email' => 'test@test.com', 'phone' => '06301234567');
	 * @return  boolean
	 */
	function setUserData(array $param) {
		$deffault = array('email', 'name', 'introduction', 'phone', 'passw');
		if(!is_null($param)) {
			$paramKeys = array_keys($param);
			$queryKeys = array_intersect($paramKeys, $deffault); // Vesszuk a ket Array metszetet
			foreach($queryKeys as $key) {
				$query[$key] = $param[$key];
			}
			if($param != $query) { // ha nem egyezik meg a ket array akkor ez egyik parameter amit megadtunk nem letezik
				throw new NotExistingParameterException();
			}
		} else {
			return false;
		}

		$db = new db();

		// Az email megfelelo?
		if(isset($query['email'])) {
			if(filter_var($query['email'], FILTER_VALIDATE_EMAIL) === FALSE) {
				throw new NotValidEmailException();
			} 

			// A mail cim egyedi?
			$db->query("SELECT id FROM USERS WHERE email = :email", array(':email' => $query['email']));
			$db->fetchAll();
			if($db->numRows() != 0) {
				throw new ExistingEmailException();	
			}
		}

		$query = self::arrayToUpper($query);

		// Osszeallitjuk a lekerdezest
		$SQL = "UPDATE USERS SET";
		$comma = "";
		foreach($query as $key => $value) {
			$SQL .= $comma . " " . $key . " = :" . $key;
			$comma = ",";
		}

		$SQL .= " WHERE ID = ".$this->_id;

		// Bind parameterek beallitasa
		$bind = array();
		foreach($query as $key => $value) {
			$bind[':'.$key] = $value;
		}

		if($db->query($SQL,$bind)) {
			return true;
		} else {
			return false;
		}

	}

	//Megvaltoztatja a user email cimet, ha a megadott jelszo helyes.
	function changeEmail($newEmail, $newEmail2, $passw){
		$db = new db();
		$db->query("SELECT id FROM USERS WHERE id = ". $this->_id ." and passw = :passw", array(':passw' => md5($passw)));

		$result = $db->fetchAll();

		if(($db->numRows() == 1) && ($newEmail == $newEmail2)) {
			$this->setUserData(array('email' => $newEmail));
			return true;
		} else {
			return false;
		}
	}
	
	//Megvaltoztatja a user jelszavat, ha a korabbi jelszo helyesen lett megadva.
	function changePassw($newPassw, $newPassw2, $oldPassw){
		// A new password megfelelo?
		if(!$newPassw || strlen($newPassw = trim($newPassw)) == 0) {
			throw new NotValidPasswordException();
		}
		
		$db = new db();
		$db->query("SELECT id FROM USERS WHERE id = ". $this->_id ." and passw = :passw", array(':passw' => md5($oldPassw)));

		$result = $db->fetchAll();

		if(($db->numRows() == 1) && ($newPassw == $newPassw2)) {
			$this->setUserData(array('passw' => md5($newPassw)));
			return true;
		} else {
			return false;
		}
	}
	
	// Egy array kulcsait nagybetusse konvertalja
	function arrayToLower(array $array){ 
		$converted_array = array();
		foreach($array as $key=>$value){
			$uppercase_key = strtolower($key);
			$converted_array[$uppercase_key] = $value;
		}
		return $converted_array; 
	}	

	// Egy array elemeit kisbetusse konvertalja
	function arrayToUpper(array $array){ 
		$converted_array = array();
		foreach($array as $key=>$value){
			$uppercase_key = strtoupper($key);
			$converted_array[$uppercase_key] = $value;
		}
		return $converted_array; 
	}

	/**
	 * Visszaadja a felhasznalo 'rangjat' a megfelelo heurisztikus keplet alapjan
	 * @return  int
	 */
	function getRate($userID) {
		$db = new db();
		$db->query("SELECT AVG(images.rate_sum / images.rate_db) AS rate FROM albums, images WHERE images.album_id = albums.id AND albums.user_id = :id", array(':id' => $userID));
		$result=$db->fetchAll();
		
		$img_rate=0;
		if ($db->numRows()==1){
			$img_rate = $result[0]['RATE'];
		}
		
		$db->query("SELECT AVG(comment_votes.value) AS avg_value FROM comments, comment_votes WHERE comment_votes.comment_id = comments.id AND comments.user_id = :id", array(':id' => $userID));
		$result=$db->fetchAll();

		$comm_rate=0;
		if ($db->numRows()==1){
			$comm_rate = $result[0]['AVG_VALUE'];
		}
		$comm_rate += 1;
		
		return (int)($img_rate*2 + $comm_rate/2);
		
	}

	/**
	 * Visszaadja az aktualis felhasznalo ID-jet
	 * @return  int
	 */
	function getUserId() {
		return $this->_id;
	}

	/**
	 * Megmondja, hogy a kapott ID-vel rendelkezo user-t koveti?
	 * @param $id user id
	 * @return  bool True, ha koveti, false ha nem
	 */
	function isFollow($id) {
		$db = new db();
		$db->query("SELECT * FROM FOLLOWS WHERE whom = :whom AND who = :who", array(":whom" => $id, ":who" => $this->_id));
		$db->fetchAll();

		if($db->numRows() > 0) {
			return true;
		} else {
			return false;
		}

	}

	function follow($id) {
		$db = new db();

		// Letezik-e a kapott ID-vel user?
		$db->query("SELECT ID FROM USERS WHERE ID = :id", array(":id" => $id));
		$db->fetchAll();
		if($db->numRows() != 1)
				throw new NotExistsUserIdException;

		$db->query("SELECT * FROM FOLLOWS WHERE WHO = :who AND WHOM = :whom", array(":whom" => $id, ":who" => $this->_id));
		$db->fetchAll();

		if($db->numRows() != 0)
			return true;

		return $db->query("INSERT INTO FOLLOWS (WHO, WHOM) VALUES (:who, :whom)", array(":whom" => $id, ":who" => $this->_id));
	}

	function unfollow($id) {
		$db = new db();

		// Letezik-e a kapott ID-vel user?
		$db->query("SELECT ID FROM USERS WHERE ID = :id", array(":id" => $id));
		$db->fetchAll();
		if($db->numRows() != 1)
				throw new NotExistsUserIdException;

		return $db->query("DELETE FROM FOLLOWS WHERE WHO = :who AND WHOM = :whom", array(":whom" => $id, ":who" => $this->_id));
	}	

	/**
	 * Visszaadja a felhasznalohoz tartozo album id-ket egy tombben
	 * @return  array
	 */
	function getAlbumIds() {
		$db = new db();
		$db->query("SELECT ID FROM ALBUMS WHERE USER_ID = ". $this->_id ." ORDER BY TITLE");
		$result = $db->fetchAll();
		return $result;
	}

	/**
	 * A felhasznalohoz letrehoz egy uj albumot a parameterul kapott neven.
	 * A sikerul akkor true, kulonben false
	 * @return  boolean
	 */
	function newAlbum($name) {
		// TODO: Kell ellenorizni, hogy van mar adott nevu albuma az adott usernek?
		
		$db = new db();
		return ($db->query("INSERT INTO ALBUMS (user_id, title) values (". $this->_id .", :name)", array(':name' => $name)));
	}

	/**
	 * Az adott ID-hez tartozo albumot torli, de csak akokr ha az album a felhasznalo tulajdonaban van!
	 * Ezt kulon le kell kezelni, ha nem akkor dobjon kivetelt.
	 * Ha sikeres a torles terjen vissza ture ertekkel, kulonben pedig false
	 * Figyelni kell, hogy rekurzivan kell majd torolni.
	 * Elobb a kepeket, meg a hozza tartozo adatokat, stb ...
	 * @param $id A torlendo aldum ID-je
	 * @return  boolean
	 */
	function deleteAlbum($id) {
		$db = new db();
		
		// Letezik-e a kapott ID-vel album?
		$db->query("SELECT ID, USER_ID FROM ALBUMS WHERE ID = :id", array(":id" => $id));
		$result=$db->fetchAll();
		if($db->numRows() != 1)
			throw new NotExistsAlbumIdException;
			
		// Ez a user a tulajdonosa az albumnak?
		if ($result[0]['USER_ID'] == $this->_id)
			throw new NotOwnedAlbumException;			
		
		// Torles	
		return ($db->query("DELETE FROM ALBUMS WHERE ID = :id", array(':id' => $id)));
	}

	/**
	 * Ha torolni akarna magat a rendszerbol a User, akkor ezt a fgv-t kell megivni.
	 * Vegig megy az osszes albumon, kepen, es torli azokat, a hozzajuk tartozo kommentekkel.
	 * A szavazasokat meg kell hagyni.
	 * Illetve magat a usert sem kell a user tablabol torolni, csak a nevet atirni mondjuk "Torolt felhasznalora" es a tobbi, szemelyes adat torolheto
	 * igy a kommentjei nem veszneke el mas felhasznalo kepei alol...
	 * @return  boolean true/false a sikeressegtol fuggoen
	 */
	function deleteUser() {
		$db = new db();
		return ($db->query("UPDATE USERS SET active=0 WHERE id=".$this->_id));
	}

	/**
	 * Random str-t general az uj jelszokhoz
	 * @param $length az uj jelszo hossza, default 8
	 * @return  str az uj random jelszo
	 */
	function generateRandStr($length=8) {
		$randstr = "";
		for($i=0; $i<$length; $i++){
			$randnum = mt_rand(0,61);
			if($randnum < 10){
				$randstr .= chr($randnum+48);
			} else if($randnum < 36){
				$randstr .= chr($randnum+55);
			} else {
				$randstr .= chr($randnum+61);
			}
		}
		return $randstr;
	}

}

?>