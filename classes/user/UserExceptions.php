<?php

// Rossz email exception
class NotValidEmailException extends Exception { }

// rosz jelszo exception
class NotValidPasswordException extends Exception { }

// Hibas adatok
class IncorrectUsernameOrPasswordException extends Exception { }

// Nemletezo user ID
class NotExistsUserIdException extends Exception { }

// Mar letezo e-mailcim az adatbazisban
class ExistingEmailException extends Exception { }

// nem letezo parameter lekerese
class NotExistingParameterException extends Exception { }

// ne letezo album ID
class NotExistsAlbumIdException extends Exception { }

// az album nincs a felhasznalo tulajdonaban
class NotOwnedAlbumException extends Exception { }

?>