<?php

include_once "AlbumExceptions.php";

class Album {

	var $_id = null; //Album ID

	/**
	 * Deffault konstruktor.
	 * Parameterul az album IG-jet kapja
	 * Ha nincs olyan akkor dobjon kivetelt
	 * Kulonben meg allitsa be a $this->_id valtozot
	 * @param $id Egy letezo Albim ID-je
	 */
	function __construct($id) {
		$db = new db();
		$db->query("SELECT ID FROM ALBUMS WHERE ID = :id", array(":id" => $id));
		$db->fetchAll();
		if($db->numRows() == 0) {
			throw new NotExistingAlbumIdException();
		} else {
			$this->_id = $id;
		}
	}

	/**
	 * Visszaadja a mappaban talalhato kepek azonositojat egy tombben
	 * @return  array
	 */
	function getImages() {
		$db = new db();
		$db->query("SELECT ID FROM IMAGES WHERE ALBUM_ID=".$this->_id);
		$result = $db->fetchAll();
		if ($db->numRows() == 0)
			throw new EmptyAlbumException();
			
		$ret = array();
		foreach ($result as $row){
			array_push($ret, $row[0]);
		}
		
		return $ret;		
	}

	/**
	 * Egy egyszeru getter metodus.
	 * Visszaadja az albim azonositojat
	 */
	function getId() {
		return $this->_id;
	}

	/**
	 * Vissza adja az album nevet
	 */
	function getTitle() {
		$db = new db();
		$db->query("SELECT title FROM ALBUMS WHERE ID = :id", array(":id" => $this->_id));
		$result = $db->fetchAll();
		return $result[0]['TITLE'];
	}

	// VIsszaadja az album elso kepenek a src-jet, az album borito kepenek erdekeben.
	// Ha nincs benne kep akkor pedig egy ures kepet
	function getFirstImage() {

	}

	/**
	 * Egy kep feltolteset kezeli le
	 * A mukodese meg megbeszeles alatt! 
	 */
	function newImage() {

	}

	/**
	 * Kitorli a megadott id-vel redelkezo kepet.
	 * Itt is rekurzivan kell torolni: a kephez tartozo kommentek, szavazasok, stb ...
	 * @param $id a kep id-je
	 * @return  boolean sikeres torles eseten true / kulonben false
	 */ 
	function deleteImage($id) {

	}

	/**
	 * Mielott a User torli az albumot meg kell hivni a ezt a fgv-t.
	 * Bejarja az albumban talalhato kepeket, es meghivja rajuk a deleteImage() fgv-t.
	 * 
	 */
	function deleteAlbum() {
		
	}
}

?>