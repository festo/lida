<?php
include_once "classes/database/OracleWrapper.class.php";
include_once "classes/resize/Resize.class.php";

function placeholder() {
	header("Content-type: image/png");
	echo file_get_contents("img/placeholder.png");
	exit;
}

// Ha nem kaptuk meg a megjelenitendo kep ID-jet mutatunk egy kiskepet
if(!isset($_GET['id'])) {
	placeholder();
}

$db = new db();
if(isset($_GET['thumb'])) {
	$db->query("SELECT thumb as image, type FROM images WHERE id = :id", array(":id" => $_GET['id']));
} else {
	$db->query("SELECT image, type FROM images WHERE id = :id", array(":id" => $_GET['id']));
}



$image = $db->fetchAll();
// Ha nincs ilyen kep akkor jelenjen meg a placeholder
if($db->numRows() != 1) {
	placeholder();
}


$imageSrc = $image[0]['IMAGE'];
/*
if(isset($_GET['thumb'])) {
	$thumb = new Resize($imageSrc, $image[0]['TYPE']);
	$thumb->resizeImage(260, 180);
	$imageSrc = $thumb->getSource();
}
*/

header("Content-type: ".$image[0]['TYPE']);
print $imageSrc;
?>