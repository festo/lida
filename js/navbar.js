$(document).ready(function(){
	var pathname = window.location.pathname;
	var URL = pathname.split("/");
	var current;
	if( URL[2] == "" ) {
		$('#home').addClass("active");
	} else {
		$('.nav-collapse ul li').each(function(i) {
			current = $(this);
			if(current.find("a").attr("href") == URL[2]) {
				current.addClass("active");

				// Ha az apja dropdown akkor a nagyapja is legyen aktiv
				if(current.parent().hasClass("dropdown-menu")) {
					current.parent().parent().addClass("active");
				}
			}
		} );
	}


});