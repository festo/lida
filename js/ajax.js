function imageVote(score, imageId) {
	$.ajax({
		type: "POST",
		url: "ajax.php",
		cache: false,
		data: { type: "imageVote", image: imageId, vote: score }
	}).done(function( msg ) {
		$('#star').raty('readOnly', true);
	});
}

function setResp(id) {
	if ( typeof setResp.resp == 'undefined' ) {
        setResp.resp = '';
    }
	if (('#resp_' + id) == setResp.resp){
		$('#inputResponse').val('');
		$(setResp.resp).removeClass('btn-primary');
		setResp.resp = '';
	} else {
		$('#inputResponse').val(id);
		$('#resp_' + id).addClass('btn-primary');
		
		if (setResp.resp != ''){
			$(setResp.resp).removeClass('btn-primary');
		}
		setResp.resp = '#resp_' + id;
	}
	poz = $('#inputResponse').offset().top;
	poz = ($(document).height() - (poz));
	$("html, body").animate({scrollTop: poz },'1000');
}

function commentVote(id, value){
	$('#inputCommID').val(id);
	$('#inputCommVote').val(value);
	$('#commentVote').submit();
}

function ImageVote(id){
	$('#inputImageID').val(id);
	$('#imageVote').submit();
}