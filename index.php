<?php
include_once "header.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<base href="/Lida/" />
	<title>Lida - Let Image DownloAd</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le styles -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">

	<link rel="stylesheet/less" type="text/css" href="css/custom.less">
	<script src="js/less-1.3.0.min.js" type="text/javascript"></script>

	<script src="js/jquery.min.js"></script>
	<script src="js/ajax.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>


	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="ico/favicon.ico">

</head>
<body>
	<div class="container">
		<div id="alert-content">
			<?php
			if(count($_Error) > 0) {
				foreach ($_Error as $error) {
					?>
					<div class="alert alert-error">
						<a class="close" data-dismiss="alert">×</a>
						<?php
							echo $error;
						?>
					</div>
					<?php
				}	
			}
			?>
		</div>
		<?php
		include_once "content.php";
		?>
		<hr>
		<footer>
			<p>&copy; Lida 2012 - All Right Reserved.<br />Created by Bódis Attila, Farkas József and Munkácsy Gergely</p>
		</footer>
	</div> <!-- /container -->
	<!-- Le javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/navbar.js"></script>
	<script src="js/jquery.raty.min.js"></script>
</body>
</html>