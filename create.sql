DROP TABLE images_and_categories;
DROP TABLE competition_vote;
DROP TABLE competition_entry;
DROP TABLE image_votes;
DROP TABLE comment_votes;
DROP TABLE visits;
DROP TABLE follows;
DROP TABLE categories;
DROP TABLE comments;
DROP TABLE competitions;
DROP TABLE LOG;
DROP TABLE images;
DROP TABLE albums;
DROP TABLE users;
DROP SEQUENCE "LIDA"."USER_SEQUENCE";
DROP SEQUENCE "LIDA"."ALBUM_SEQUENCE";
DROP SEQUENCE "LIDA"."IMAGE_SEQUENCE";
DROP SEQUENCE "LIDA"."COMPETITION_SEQUENCE";
DROP SEQUENCE "LIDA"."COMMENT_SEQUENCE";
DROP SEQUENCE "LIDA"."CATEGORY_SEQUENCE";
DROP SEQUENCE "LIDA"."LOG_SEQUENCE";

CREATE SEQUENCE  "LIDA"."USER_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE  "LIDA"."ALBUM_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE  "LIDA"."IMAGE_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE  "LIDA"."COMPETITION_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE  "LIDA"."COMMENT_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE  "LIDA"."CATEGORY_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE  "LIDA"."LOG_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

CREATE TABLE  users  (
        id  NUMBER NOT NULL,
        email  VARCHAR(100),
        name  VARCHAR(100),
        passw  VARCHAR(32),
        introduction  VARCHAR(1000),
        phone  VARCHAR(20)  ,
		active CHAR DEFAULT 1 check (active in (0,1)),
        PRIMARY KEY ( id )
);

CREATE TABLE  images  (
        id  NUMBER NOT NULL  ,
        album_id  NUMBER  NOT NULL,
        type  VARCHAR(20)  ,
        image  BLOB  ,
        thumb  BLOB  ,
        uploaded_time  TIMESTAMP  ,
        title  VARCHAR(100)  ,
        text   VARCHAR(1000)  ,
        RATE_SUM NUMBER   ,
        rate_db  NUMBER  ,
        PRIMARY KEY ( id )
);

CREATE TABLE  albums  (
        id  NUMBER NOT NULL  ,
        user_id  NUMBER  NOT NULL,
        title  VARCHAR(100)  ,
        PRIMARY KEY ( id )
);

CREATE TABLE  categories  (
        id  NUMBER NOT NULL  ,
        name  VARCHAR(100)  ,
        PRIMARY KEY ( id )
);

CREATE TABLE  comments  (
        id  NUMBER NOT NULL  ,
        image_id  NUMBER  NOT NULL,
        user_id  NUMBER  NOT NULL,
        response  NUMBER  ,
        text  VARCHAR(255)  ,
        when  TIMESTAMP  , 
        rate  DECIMAL  ,
        PRIMARY KEY ( id )
);

CREATE TABLE  follows  (
        who  NUMBER,
        whom  NUMBER,
        PRIMARY KEY ( who ,  whom )
);

CREATE TABLE  comment_votes  (
        comment_id  NUMBER  NOT NULL,
        user_id  NUMBER  NOT NULL,
        value  NUMBER(1),
        PRIMARY KEY ( user_id ,  comment_id )
);

CREATE TABLE  image_votes  (
        image_id  NUMBER  NOT NULL,
        user_id  NUMBER  NOT NULL,
        value  NUMBER(1) ,
        PRIMARY KEY ( image_id ,  user_id )
);

CREATE TABLE  competitions  (
        id  NUMBER NOT NULL  ,
        user_id  NUMBER  NOT NULL,
        start_date  TIMESTAMP  ,
        end_date  TIMESTAMP  ,
        title  VARCHAR(100)  ,
        text   VARCHAR(1000)  ,
        winner NUMBER,
        PRIMARY KEY ( id )
);

CREATE TABLE  competition_entry  (
        competition_id  NUMBER  ,
        image_id  NUMBER  ,
        PRIMARY KEY ( competition_id ,  image_id )
);

CREATE TABLE  competition_vote  (
        competition_id  NUMBER NOT NULL  ,
        user_id  NUMBER  ,
        image_id  NUMBER  ,
        PRIMARY KEY ( competition_id ,  user_id ,  image_id )
);

CREATE TABLE  visits  (
        user_id  NUMBER  NOT NULL,
        image_id  NUMBER  NOT NULL,
        when  TIMESTAMP  ,
        PRIMARY KEY ( user_id ,  image_id ,  when )
);

CREATE TABLE  LOG  (
      	id	NUMBER	NOT NULL,
        user_id  NUMBER  NOT NULL,
        when  TIMESTAMP  ,
        EVENT  VARCHAR(255)  ,
        PRIMARY KEY ( id )
);

CREATE TABLE  images_and_categories  (
        image_id  NUMBER NOT NULL  ,
        category_id  NUMBER  NOT NULL	,
        PRIMARY KEY ( image_id ,  category_id )
);

ALTER TABLE  follows  ADD FOREIGN KEY (who) REFERENCES  users  ( id );
ALTER TABLE  follows  ADD FOREIGN KEY (whom) REFERENCES  users  ( id );
ALTER TABLE  images  ADD FOREIGN KEY (album_id) REFERENCES  albums  ( id );
ALTER TABLE  albums  ADD FOREIGN KEY (user_id) REFERENCES  users  ( id );
ALTER TABLE  comments  ADD FOREIGN KEY (image_id) REFERENCES images (id);
ALTER TABLE  comments  ADD FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE  comments  ADD FOREIGN KEY (response) REFERENCES comments (id);
ALTER TABLE  comment_votes  ADD FOREIGN KEY (comment_id) REFERENCES  comments  ( id );
ALTER TABLE  comment_votes  ADD FOREIGN KEY (user_id) REFERENCES  users  ( id );
ALTER TABLE  image_votes  ADD FOREIGN KEY (image_id) REFERENCES  images  ( id );
ALTER TABLE  image_votes  ADD FOREIGN KEY (user_id) REFERENCES  users  ( id );
ALTER TABLE  competitions  ADD FOREIGN KEY (user_id) REFERENCES  users  ( id );
ALTER TABLE  competition_entry  ADD FOREIGN KEY (competition_id) REFERENCES  competitions  ( id );
ALTER TABLE  competition_entry  ADD FOREIGN KEY (image_id) REFERENCES  images  ( id );
ALTER TABLE  competition_vote  ADD FOREIGN KEY (competition_id) REFERENCES  competitions  ( id );
ALTER TABLE  competition_vote  ADD FOREIGN KEY (user_id) REFERENCES  users  ( id );
ALTER TABLE  competition_vote  ADD FOREIGN KEY (image_id) REFERENCES  images  ( id );
ALTER TABLE  visits  ADD FOREIGN KEY (user_id) REFERENCES  users  ( id );
ALTER TABLE  visits  ADD FOREIGN KEY (image_id) REFERENCES  images  ( id );
ALTER TABLE  log  ADD FOREIGN KEY (user_id) REFERENCES  users  ( id );
ALTER TABLE  images_and_categories  ADD FOREIGN KEY (image_id) REFERENCES  images  ( id );
ALTER TABLE  images_and_categories  ADD FOREIGN KEY (category_id) REFERENCES  categories  ( id );

--Auto increment for users.id
CREATE OR REPLACE TRIGGER "LIDA"."USER_TRIGGER" 
BEFORE INSERT
ON users
FOR EACH ROW
BEGIN
SELECT user_sequence.nextval INTO :NEW.ID FROM dual;
END;
/
ALTER TRIGGER "LIDA"."USER_TRIGGER" ENABLE;

--Auto increment for albums.id
CREATE OR REPLACE TRIGGER "LIDA"."ALBUM_TRIGGER" 
BEFORE INSERT
ON albums
FOR EACH ROW
BEGIN
SELECT album_sequence.nextval INTO :NEW.ID FROM dual;
END;
/
ALTER TRIGGER "LIDA"."ALBUM_TRIGGER" ENABLE;

--Auto increment for images.id
CREATE OR REPLACE TRIGGER "LIDA"."IMAGE_TRIGGER" 
BEFORE INSERT
ON images
FOR EACH ROW
BEGIN
SELECT image_sequence.nextval INTO :NEW.ID FROM dual;
END;
/
ALTER TRIGGER "LIDA"."IMAGE_TRIGGER" ENABLE;

--Auto increment for competitions.id
CREATE OR REPLACE TRIGGER "LIDA"."COMPETITION_TRIGGER" 
BEFORE INSERT
ON competitions
FOR EACH ROW
BEGIN
SELECT competition_sequence.nextval INTO :NEW.ID FROM dual;
END;
/
ALTER TRIGGER "LIDA"."COMPETITION_TRIGGER" ENABLE;

--Auto increment for comments.id
CREATE OR REPLACE TRIGGER "LIDA"."COMMENT_TRIGGER" 
BEFORE INSERT
ON comments
FOR EACH ROW
BEGIN
SELECT comment_sequence.nextval INTO :NEW.ID FROM dual;
END;
/
ALTER TRIGGER "LIDA"."COMMENT_TRIGGER" ENABLE;

--Auto increment for categories.id
CREATE OR REPLACE TRIGGER "LIDA"."CATEGORY_TRIGGER" 
BEFORE INSERT
ON categories
FOR EACH ROW
BEGIN
SELECT category_sequence.nextval INTO :NEW.ID FROM dual;
END;
/
ALTER TRIGGER "LIDA"."CATEGORY_TRIGGER" ENABLE;

--Auto increment for log.id
CREATE OR REPLACE TRIGGER "LIDA"."LOG_TRIGGER" 
BEFORE INSERT
ON log
FOR EACH ROW
BEGIN
SELECT log_sequence.nextval INTO :NEW.ID FROM dual;
END;
/
ALTER TRIGGER "LIDA"."LOG_TRIGGER" ENABLE;

--Create default album for the user
CREATE or REPLACE TRIGGER "LIDA"."NEW_USER_TRIGGER"
AFTER INSERT
ON users
FOR EACH ROW 
BEGIN
        INSERT INTO LIDA.ALBUMS (user_id, title) values (:NEW.id, 'Default');
END;
/
ALTER TRIGGER "LIDA"."NEW_USER_TRIGGER" ENABLE;

--Kiszámolja az egyes képeknek a pontját
CREATE OR REPLACE TRIGGER "LIDA"."image_rate_refresh" AFTER INSERT ON IMAGE_VOTES
for each row
declare
  szum images.rate_sum%type;
  db images.rate_db%type;
begin
 select NVL(rate_sum,0), NVL(rate_db,0) into szum, db from images where id = :NEW.image_id;
 db:=db+1;
 szum:=szum + :NEW.value;
 update images set rate_sum = szum, rate_db = db where id = :NEW.image_id;
end;
/
ALTER TRIGGER "LIDA"."image_rate_refresh" ENABLE;

--Logolja, hogy van új user
CREATE OR REPLACE TRIGGER "LIDA"."log_new_user" AFTER INSERT ON users
for each row
declare
begin
    insert into lida.log (user_id, when, event) values(:NEW.ID,systimestamp,concat(concat('ID: ',:NEW.ID),' felhasznalo beregisztralt.'));
end;
/
ALTER TRIGGER "LIDA"."log_new_user" ENABLE;

--Logolja, hogy van új kép
CREATE OR REPLACE TRIGGER "LIDA"."log_new_image" AFTER INSERT ON images
for each row
declare
	azon users.id%type;
begin
	SELECT USER_ID INTO AZON FROM ALBUMS WHERE ALBUMS.ID=:NEW.ALBUM_ID;
    insert into lida.log (user_id, when, event) values(azon,systimestamp,concat(concat(concat('ID: ', azon), ' felhasznalo feltoltott egy uj kepet('),concat(:NEW.id,').')));
end;
/
ALTER TRIGGER "LIDA"."log_new_image" ENABLE;

--Logolja, hogy van új album
CREATE OR REPLACE TRIGGER "LIDA"."log_new_album" AFTER INSERT ON albums
for each row
declare
BEGIN
    insert into lida.log (user_id, when, event) values(:NEW.user_id,systimestamp,concat(concat(concat('ID: ',:NEW.user_id), ' felhasznalo keszitett egy uj albumot('),concat(:NEW.id,').')));
end;
/
ALTER TRIGGER "LIDA"."log_new_album" ENABLE;

--Logolja, hogy töröltek egy albumot
CREATE OR REPLACE TRIGGER "LIDA"."log_del_album" AFTER DELETE ON albums
for each row
declare
BEGIN
    insert into lida.log (user_id, when, event) values(:OLD.user_id,systimestamp,concat(concat(concat('ID: ',:OLD.user_id), ' felhasznalo torolt egy albumot('),concat(:OLD.id,').')));
end;
/
ALTER TRIGGER "LIDA"."log_del_album" ENABLE;

--Logolja, hogy új pályázatot hoztak létre
CREATE OR REPLACE TRIGGER "LIDA"."log_new_competition" AFTER INSERT ON competitions
for each row
declare
BEGIN
    insert into lida.log (user_id, when,event) values(:NEW.user_id,systimestamp,concat(concat(concat('ID: ',:NEW.user_id), ' felhasznalo keszitett egy uj palyazatot('),concat(:NEW.id,').')));
end;
/
ALTER TRIGGER "LIDA"."log_new_competition" ENABLE;

--Logolja, hogy létrejött egy új komment
CREATE OR REPLACE TRIGGER "LIDA"."log_new_comment" AFTER INSERT ON comments
for each row
declare
BEGIN
    insert into lida.log (user_id, when, event) values(:NEW.user_id,systimestamp,concat(concat(concat(concat('ID: ',:NEW.user_id), ' hozzaszolt egy kephez('),concat(:NEW.image_id,'). Komment id: ')),:nEW.id));
end;
/
ALTER TRIGGER "LIDA"."log_new_comment" ENABLE;

--Logolja, hogy szavaztak egy kommentre
CREATE OR REPLACE TRIGGER "LIDA"."log_new_comment_vote" AFTER INSERT ON comment_votes
for each row
declare
nem VARCHAR2(4);
begin
    nem:='';
    if(:NEW.value=-1) then
      nem:=' nem';
    end if;
    insert into lida.log (user_id, when, event) values(:NEW.user_id,systimestamp,concat(concat(concat(:NEW.user_id, '-nak/-nek'),concat(nem,' tetszik egy komment (id):')),:new.comment_id));
end;
/
ALTER TRIGGER "LIDA"."log_new_comment_vote" ENABLE;

--Logolja, hogy szavaztak egy képre
CREATE OR REPLACE TRIGGER "LIDA"."log_new_image_vote" AFTER INSERT ON image_votes
for each row
declare
begin
    insert into lida.log (user_id, when, event) values(:NEW.user_id,systimestamp,concat(concat(:NEW.user_id, 'szavazott egy kepre: '),:new.image_id));
end;
/
ALTER TRIGGER "LIDA"."log_new_image_vote" ENABLE;

--Komment törléséhez a függőségek feloldása
CREATE OR REPLACE TRIGGER "LIDA"."del_comment" BEFORE DELETE ON comments
for each row
declare
BEGIN
    delete from comment_votes where :OLD.id=comment_id; 
end;
/
ALTER TRIGGER "LIDA"."del_comment" ENABLE;

--Kép törléséhez a függőségek feloldása
CREATE OR REPLACE TRIGGER "LIDA"."del_image" BEFORE DELETE ON images
for each row
declare
BEGIN
    delete from image_votes where :OLD.id=image_id; 
    delete from comments where :OLD.id=image_id;
	delete from images_and_categories where :OLD.id=image_id;
	delete from visits where :OLD.id=image_id;
	delete from competition_vote where :OLD.id=image_id;
	delete from competition_entry where :OLD.id=image_id;
end;
/
ALTER TRIGGER "LIDA"."del_image" ENABLE;

--Album törléséhez a függőségek feloldása
CREATE OR REPLACE TRIGGER "LIDA"."del_album" BEFORE DELETE ON albums
for each row
declare
BEGIN
    delete from images where :OLD.id=album_id; 
end;
/
ALTER TRIGGER "LIDA"."del_album" ENABLE;

--Felhasználó "törlése"
CREATE OR REPLACE TRIGGER "LIDA"."to_inactive" BEFORE UPDATE OF active ON users
FOR EACH ROW
when (NEW.active=0)
declare
BEGIN
    DELETE FROM ALBUMS WHERE :OLD.ID=USER_ID;
    :NEW.name:=concat(:NEW.name,' (törölt felhasználó)');
end;
/
ALTER TRIGGER "LIDA"."to_inactive" ENABLE;

-- Visszaadja az utoljara beszurt Id erteket a megadott parameteru seqence-bol. Fontos, hogy kozvetlen insert utasitas utan kell megivni.
create or replace function getLastGeneratedId(cTableName in Varchar2) return number 
  is 
    v_result number; 
    begin 
    declare v_sqlstmt varchar2(200); 
    begin v_sqlstmt := 'SELECT ' || cTableName || '_SEQUENCE.currval from dual'; 
    execute immediate v_sqlstmt into v_result; 
    return v_result; 
    end; 
end getLastGeneratedId; 
/

-- Users
INSERT INTO LIDA.USERS (EMAIL,NAME,PASSW,INTRODUCTION,PHONE) VALUES ('test@test.com','test','098f6bcd4621d373cade4e832627b4f6',NULL,NULL);
Insert into LIDA.USERS (EMAIL,NAME,PASSW,INTRODUCTION,PHONE) values ('gergely.munkacsy@gmail.com','Munkácsy Gergely','0c1ca2a2bd08074213d5041ac82980a4','Hali!','+36302473747');
INSERT INTO LIDA.USERS (EMAIL,NAME,PASSW,INTRODUCTION,PHONE) VALUES ('bodis.attila@gmail.com','Bódis Attila','49ea86584c5e21fc1305ff22f0357f22','Ez és ez vagyok én... :)','+36306479688');
Insert into LIDA.USERS (EMAIL,NAME,PASSW,INTRODUCTION,PHONE) values ('fjozsef001@gmail.com','Farkas József','5b2dfe248a3cbb7f9a42dfaf082488f3',null,'+36302701971');

-- Follow
INSERT INTO "LIDA"."FOLLOWS" (WHO, WHOM) VALUES ('2', '3');
INSERT INTO "LIDA"."FOLLOWS" (WHO, WHOM) VALUES ('2', '1');

--Categories
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Absztrakt');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Divat');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Egyéb');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Ember');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Gyümölcsök');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Sport');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Állatok');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Természet');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Utazás');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Vicces');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Tudományos');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Művészeti');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Család');
INSERT INTO LIDA.CATEGORIES(NAME) VALUES ('Szerelem');


COMMIT;