<?php

include_once "classes/database/OracleWrapper.class.php";
include_once "classes/resize/Resize.class.php";

$db = new db();

// innen tolti be  akepeket
$dir = "kepek";
$files = scandir($dir);

$database = oci_connect("lida", "lida", "xe");

//lekerdezem a mar meglevo albumokat egy tombe, es azok kozul valasztok veletlenul egyet
$stmt = oci_parse($database, "SELECT ID FROM ALBUMS");
oci_execute($stmt);
$ret = null;
$rowCount = oci_fetch_all($stmt, $ret, null, null, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);

$ids = array();
foreach ($ret as $row) {
	$row = array_values($row);
	array_push($ids, $row[0]);
}

//lekerdezem a kategoriakat es azt is minden kep kap randomra
$stmt = oci_parse($database, "SELECT ID FROM CATEGORIES");
oci_execute($stmt);
$ret = null;
$rowCount = oci_fetch_all($stmt, $ret, null, null, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);

$catIds = array();
foreach ($ret as $row) {
	$row = array_values($row);
	array_push($catIds, $row[0]);
}

foreach($files as $image) {
	if($image != '.' && $image != '..') {
		$fp = fopen($dir.'/'.$image, 'r');
		$type = getimagesize($dir.'/'.$image);
		$stmt = oci_parse($database, "INSERT INTO images (album_id, type, image, uploaded_time, title, text) VALUES (".$ids[array_rand($ids)].", :type, EMPTY_BLOB(), systimestamp, 'Title', 'Text') RETURNING image INTO :image");
		$newlob = oci_new_descriptor($database, OCI_D_LOB);

		oci_bind_by_name($stmt, ":image", $newlob, -1, OCI_B_BLOB);
		oci_bind_by_name($stmt, ":type", $type['mime']);

		oci_execute($stmt,OCI_DEFAULT);

		$fileBlob = file_get_contents($dir.'/'.$image);

		$newlob->save($fileBlob);
		oci_commit($database);
		// cleanup
		$newlob->free();
		oci_free_statement($stmt);
		echo $image." sikeresen hozzaadva!<br/>";

		//lekerdezzuk mit szzurtunk be utoljara
		$db->query("select getLastgeneratedid('image') as ID from dual");
		$image = $db->fetchAll();

		//A kiskepet is elmentjuk
		$thumb = new Resize($fileBlob, $type['mime']);
		$thumb->resizeImage(260, 180);
		$imageSrc = $thumb->getSource();

		$stmt = oci_parse($database, "UPDATE images SET thumb = EMPTY_BLOB() WHERE id = ".$image[0]['ID']." RETURNING thumb INTO :thumb");
		$newlob = oci_new_descriptor($database, OCI_D_LOB);
		oci_bind_by_name($stmt, ":thumb", $newlob, -1, OCI_B_BLOB);
		oci_execute($stmt,OCI_DEFAULT);
		$newlob->save($imageSrc);
		oci_commit($database);
		$newlob->free();
		oci_free_statement($stmt);

		// megkapja a kategoriat is
		$db->query("INSERT INTO images_and_categories (image_id, category_id) VALUES (:image_id, :category_id)", array(":image_id" => $image[0]['ID'], ":category_id" => $catIds[array_rand($catIds)]));
		

	}
}





?>