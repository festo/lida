<?php

if(!$_SESSION['is_logged']) {
	// Ha nincs bejelentkezve
	switch ($URL[0]) {
		default:
		case 'login':
			include_once "pages/login.php";	
		break;

		case 'successRegistration':
			include_once "pages/successRegistration.php";	
		break;

		case 'registration':
			include_once "pages/registration.php";	
		break;
		
		case 'forget':
			include_once "pages/forget.php";	
		break;
	}

} else {

	// menu betoltese
	include_once "pages/navbar.php";

	// el kell donteni, hogy milyen keres erkezett
	if(!isset($URL[0])) {
	// ha nincs beallitva semmi akokr alapertelmezett betoltese
		include_once "pages/dashboard.php";

	} elseif(isset($URL[0]) && file_exists("pages/".$URL[0].".php")) {
	// ha van, es letezik a fajl, betoltjuk
		include_once "pages/".$URL[0].".php";

	} else {
	// kulonben betoltunk egy 404-es oldalt
		include_once "pages/404.php";
	}


}

?>