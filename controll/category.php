<?php

// ha nics megadva category ID
if(!isset($URL[1])) {
	header("location: browse");
	exit;	
}

$db = new db();

// A ketagoriak lekerdezese
$db->query("SELECT * FROM CATEGORIES ORDER BY NAME");
$categories = $db->fetchAll();

//A megadott kategoriaba eso kepek lekerdezese
$db->query("select image_id as ID from images_and_categories where category_id = :category_id", array(":category_id" => $URL[1]));
$categoryImages = $db->fetchAll();


// ha nincs egy darab se  akategoriaban akkor vissza
if($db->numRows() < 1) {
	header("location: ../browse");
	exit;
}

// A kategoria nevenek a lekerdezese
$db->query("select name from categories where id = :category_id", array(":category_id" => $URL[1]));
$category = $db->fetchAll();
$category = $category[0]; // egy kis szepites a kodban

?>