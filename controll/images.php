<?php
$db = new db();

// Csak akkor jelenjenek meg itt a kepek, ha en avgyok a megadott album tulajdonosa!
$db->query("SELECT ID, TITLE FROM ALBUMS WHERE ID = :id", array(":id" => (int)$URL[1]));

$album = $db->fetchAll();
$album = $album[0];

if($db->numRows() != 1) {
	header("location: ../myAlbums");
	exit;
}

// Ha a user tulajdona az album, akor lekerdezzuk az albumba tartozo kepeket
$db->query("SELECT ID FROM IMAGES WHERE ALBUM_ID = :id",  array(":id" => (int)$URL[1]));
$images = $db->fetchAll();

?>