<?php
// ha nem kapott parametert
if(!isset($URL[1])) {
	header("location: browse");
	exit;	
} else if($URL[1] == '') {
	header("location: ../browse");
	exit;
}

$db = new db();

//Ha kaptunk $_POST erteket
if(isset($_POST['q']) && $_POST['q'] == "edit-image") {
	$data = array(
		":title" => $_POST['title'],
		":user_text" => $_POST['text'],
		":id" => $URL[1]	// Ez itt egy sebezhetoseg, de most figyelmen kivul hagyjuk
		);
	$db->query("UPDATE images set title = :title, text = :user_text where id = :id", $data);

	// kategoriak elmentese
	// eldobjuk amit eddig bevittunk es ujra beszurjuk oket
	$db->query("delete from images_and_categories where image_id = :image_id", array(":image_id" => $URL[1]));
	if(isset($_POST['cat'])) {
		foreach($_POST['cat'] as $id) {
			$db->query("insert into images_and_categories (image_id, category_id) values (:image_id, :category_id)", array(":image_id" => $URL[1], ":category_id" => $id));
		}
	}

}

// ha nem az en tulajdonom a kep?
$db->query("select images.album_id as album, images.title, images.text from images, albums where albums.id = images.album_id and albums.user_id = :user_id and images.id = :image_id", array(":user_id" => $_User->getUserId(), ":image_id" => $URL[1]));
$imageData = $db->fetchAll();

// Ha  nem egy ilyen sor van akkor visszadobjuk nezelodani
if($db->numRows() != 1) {
	header("location: ../browse");
	exit;
}

$imageData = $imageData[0];

// Kategoriak lekerdezese a listahoz
$db->query("select * from categories order by name");
$categories = $db->fetchAll();

// A már kiválasztott kategoriak lekerdezese
$db->query("SELECT category_id as id FROM images_and_categories where image_id = :image_id", array(":image_id" => $URL[1]));
$selected = $db->fetchAll();

// csak az ID-k kellenek nekunk
$selectedCategories = array();
foreach($selected as $cat) {
	$selectedCategories[] = $cat['ID'];
}
?>