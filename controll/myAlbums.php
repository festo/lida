<?php

$albumIds = $_User->getAlbumids();

//ha kaptunk POST adatokat akkor lekezeljuk
if(isset($_POST['q'])) {
	if($_User->newAlbum($_POST['albumInput'])) {
		header("Location: myAlbums");
		exit;
	} else {
		$_Error[] = "Nem sikerült létrehozni az albumot.";
	}
}

/**
 * Az album elso kepenek az ID-jet adja vissza a boritokephez
 */
function firstImage($albumId) {
	$db = new db();
	$db->query("select id from images where album_id = :album_id AND ROWNUM < 2 order by uploaded_time", array("album_id" => (int)$albumId));
	$result = $db->fetchAll();
	if($db->numRows() != 1) {
		// A show_image.php ha olyan ID-t kap ami nem letezik akkor a placeholder lesz az a kep, amit megjelenit
		return 0;
	} else {
		return $result[0]['ID'];
	}
}

?>