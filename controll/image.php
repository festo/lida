<?php

// ha nem kapott parametert
if(!isset($URL[1])) {
	header("location: browse");
	exit;	
} else if($URL[1] == '') {
	header("location: ../browse");
	exit;
}
// kulonben folytatjuk

$db = new db();
//lekerdezzuk a kepet, hogy letezik-e egyaltalan
$db->query("select images.id AS id, images.title AS title, text, RATE_SUM, RATE_db, albums.title AS album_title, album_id, user_id, name from images, albums, users where images.id = :id AND albums.id = album_id AND users.id = user_id", array(":id" => $URL['1']));
$imageData = $db->fetchAll(); // elmentjuk a kep adatait
$imageData = $imageData[0];
if($db->numRows() != 1) {
	// visszadobjuk, had nezelodjon tovabb
	header("location: ../browse");
	exit;
}

// A statisztikai resz elkeszitese
//Egy kep megjelenitesekor el kell menteni azt, hogy azt a user megnezte
$db->query("insert into visits (user_id, image_id, when) values (:user_id, :image_id, systimestamp)", array(":user_id" => $_User->getuserId(), ":image_id" => (int)$URL['1']));


//kiszamoljuk a kep szavaati erteket
if($imageData['RATE_DB'] != null) {
	$imageRate = $imageData['RATE_SUM'] / $imageData['RATE_DB'];
} else {
	$imageRate = 0;
}

// megnezzuk, hogy a user szavazott-e mar a kepre, mert ha igen, akkor csak read onli a szavazas
$db->query("select * from image_votes where image_id = :image_id and user_id = :user_id", array(":image_id" => $URL['1'], ":user_id" => $_User->getuserId()));
$db->fetchAll();
$isVote = $db->numRows();

if(isset($_POST['q'])) {
	switch ($_POST['q']){
		case 'comment':
			if ($_POST['inputResponse'] != ""){
				$db->query("insert into comments (image_id, user_id, response, when, text) values (:image_id, :user_id, :response, systimestamp, :text)", array(":image_id" => (int)$URL['1'], ":user_id" => $_User->getuserId(), ':response' => $_POST['inputResponse'], ':text' => $_POST['inputComment']));
			} else {
				$db->query("insert into comments (image_id, user_id, when, text) values (:image_id, :user_id, systimestamp, :text)", array(":image_id" => (int)$URL['1'], ":user_id" => $_User->getuserId(), ':text' => $_POST['inputComment']));
			}
			break;
		case 'commentVote':
			$db->query("SELECT * FROM comment_votes WHERE comment_id = :comment_id AND user_id = :user_id", array(":comment_id" => $_POST['inputCommID'], ":user_id" => $_User->getuserId()));
			$db->fetchAll();
			if ($db->numRows() == 0){
				$db->query("insert into comment_votes (comment_id, user_id, value) values (:comment_id, :user_id, :value)", array(":comment_id" => $_POST['inputCommID'], ":user_id" => $_User->getuserId(), ':value' => $_POST['inputCommVote']));
			}
			break;
	}
}
?>