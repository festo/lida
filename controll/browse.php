<?php

$db = new db();

// A ketagoriak lekerdezese
$db->query("SELECT * FROM CATEGORIES ORDER BY NAME");
$categories = $db->fetchAll();

// ajanlott kepek lekerdezese
$db->query("select * from (SELECT CATEGORIES.ID FROM CATEGORIES, VISITS, IMAGES_AND_CATEGORIES WHERE ROWNUM<=1000 AND USER_ID=:user_id AND VISITS.IMAGE_ID=IMAGES_AND_CATEGORIES.IMAGE_ID AND IMAGES_AND_CATEGORIES.CATEGORY_ID=CATEGORIES.ID GROUP BY CATEGORIES.ID order by count(CATEGORIES.ID) DESC) where rownum<=3", array(":user_id" => $_User->getuserid()));

$recommendedCategories = $db->fetchAll();
// ha van legalabb egy ilyen ID
if($db->numRows() > 0) {

	// egy str listaba fuzzuk az ID-jet a lekerdezeshez
	$categoryList = "";
	foreach($recommendedCategories as $cat) {
		$categoryList .= $cat['ID'].", ";
	}

	$categoryList = substr($categoryList, 0, -2);

	// lekerdezzuk a magadott ID ba beleeseo kepeket
	$db->query("select image_id as id from images_and_categories WHERE ROWNUM < 7 and images_and_categories.category_id in (".$categoryList.") order by dbms_random.VALUE");
} else {
	// ha nincs egy Id se, akkor random kep lesz az adatbazisbol
	$db->query("select id from (select id from images order by dbms_random.VALUE)WHERE ROWNUM < 7");
}

$recommendedImages = $db->fetchAll();

//A veletlen kepek lekerdezese
$db->query("select id from (select id from images order by dbms_random.VALUE)WHERE ROWNUM < 7");
$randomImages = $db->fetchAll();
?>