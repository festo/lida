<?php

//ha kaptunk POST adatokat akkor lekezeljuk
if(isset($_POST['q'])) {
	try {
		if($_User->resetPassw($_POST['emailInput'])) {
			header("Location: login");
			exit;
		} else {
			$_Error[] = "Hiba történt a művelet folyamán!";
		}
	} catch(NotValidEmailException $e) {
		$_Error[] = "Nem megfelelő e-mail cím!";
	} catch(NotExistsUserIdException $e) {
		$_Error[] = "Nem létezik ilyen e-mail cím az adatbázisunkban!";
	}
}


?>