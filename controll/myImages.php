<?php
$db = new db();

function uploadImage($file, $albumId) {
	include_once "classes/resize/Resize.class.php";
	global $db;

	$database = oci_connect("lida", "lida", "xe");
	$stmt = oci_parse($database, "INSERT INTO images (album_id, type, image, uploaded_time, title, text) VALUES (".$albumId.", :type, EMPTY_BLOB(), systimestamp, '', '') RETURNING image INTO :image");
	$newlob = oci_new_descriptor($database, OCI_D_LOB);

	oci_bind_by_name($stmt, ":image", $newlob, -1, OCI_B_BLOB);
	oci_bind_by_name($stmt, ":type", $file['type']);

	oci_execute($stmt,OCI_DEFAULT);

	$fileBlob = file_get_contents($file['tmp_name']);

	$newlob->save($fileBlob);
	oci_commit($database);
		// cleanup
	$newlob->free();
	oci_free_statement($stmt);

		//lekerdezzuk mit szzurtunk be utoljara
	$db->query("select getLastgeneratedid('image') as ID from dual");
	$image = $db->fetchAll();

		//A kiskepet is elmentjuk
	$thumb = new Resize($fileBlob, $file['type']);
	$thumb->resizeImage(260, 180);
	$imageSrc = $thumb->getSource();

	$stmt = oci_parse($database, "UPDATE images SET thumb = EMPTY_BLOB() WHERE id = ".$image[0]['ID']." RETURNING thumb INTO :thumb");
	$newlob = oci_new_descriptor($database, OCI_D_LOB);
	oci_bind_by_name($stmt, ":thumb", $newlob, -1, OCI_B_BLOB);
	oci_execute($stmt,OCI_DEFAULT);
	$newlob->save($imageSrc);
	oci_commit($database);
	$newlob->free();
	oci_free_statement($stmt);

	return $image[0]['ID'];

}

if(isset($_POST['type'])) {
	switch ($_POST['type']) {
		case 'edit':
			//Album atnevezese
			if(isset($_POST['albumId']) && $_POST['title'] != "") {
				$db->query("UPDATE ALBUMS SET TITLE = :title WHERE id = :id", array(":title" => $_POST['title'], ":id" => $_POST['albumId']));
			}
		break;

		case 'upload':
			$types = array('image/gif', 'image/jpeg', 'image/png');
			if(!in_array($_FILES["file"]['type'], $types)) {
				$_Error[] = "Nem támogatott fájlformátum!";
				break;
			}
			$id = uploadImage($_FILES["file"], $_POST['albumId']);
			header("location: ../editImage/".$id);
			exit;
		break;
	}
}


// Csak akkor jelenjenek meg itt a kepek, ha en avgyok a megadott album tulajdonosa!
$db->query("SELECT ID, TITLE FROM ALBUMS WHERE ID = :id AND USER_ID = :user_id", array(":id" => (int)$URL[1], ":user_id" => $_User->getUserId()));

$album = $db->fetchAll();
$album = $album[0];

if($db->numRows() != 1) {
	header("location: ../myAlbums");
	exit;
}

// Ha a user tulajdona az album, akor lekerdezzuk az albumba tartozo kepeket
$db->query("SELECT ID FROM IMAGES WHERE ALBUM_ID = :id",  array(":id" => (int)$URL[1]));
$images = $db->fetchAll();

?>