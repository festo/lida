<?php

try{
	if(isset($_POST['q'])) {
		switch ($_POST['q']) {
			case 'setDeffaultData':
				$array = array(
					'name' => $_POST['inputName'],
					'phone' => $_POST['inputPhone'],
					'introduction' => $_POST['inputIntroduction']
					);
				$_User->setUserData($array);
			break;
			case 'changeEmail':
				if (!$_User->changeEmail($_POST['inputEmail'], $_POST['inputEmail2'], $_POST['inputPassw'])){
					$_Error[] = "Hibás adat!";
				}
			break;
			case 'changePass':
				if (!$_User->changePassw($_POST['inputNewPassw'], $_POST['inputNewPassw2'], $_POST['inputOldPassw'])){
					$_Error[] = "Hibás adat!";
				}
			break;

			default:
			break;
		}
	}
} catch(NotExistingParameterException $e) {
	$_Error[] = "<b>Rendszerhiba!</b> Nemlétező parameterek!";
} catch(NotValidEmailException $e) {
	$_Error[] = "Nem megfelelő e-mail cím!";
} catch(ExistingEmailException $e) {
	$_Error[] = "Már létező e-mail cím!";
}

try {
	$userData = $_User->getUserData();
} catch(NotExistingParameterException $e) {
	$_Error[] = "<b>Rendszerhiba!</b> Nemlétező parameterek!";
}
	
?>