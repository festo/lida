<?php

// ha nem kapott parametert
if(!isset($URL[1])) {
	header("location: browse");
	exit;	
} else if($URL[1] == '') {
	header("location: ../browse");
	exit;
}
// kulonben folytatjuk

$db = new db();

$db->query("select id, TO_CHAR(end_date, 'YYYY-MM-DD') as end_date, TO_CHAR(start_date, 'YYYY-MM-DD') as start_date, text, title from competitions where id = :id", array(":id" => $URL[1]));
$competition = $db->fetchAll();

if($db->numRows() != 1) {
	header("location: ../browse");
	exit;	
}

if(isset($_POST['q'])) {
	if($_POST['q'] == "sendImage") {
		foreach($_POST['img'] as $img) {
			$db->query("select * from competition_entry where competition_id = :competition_id AND image_id = :image_id", array(":competition_id" => $URL[1], ":image_id" => $img));
			$db->fetchAll();
			if($db->numRows() > 0) {
				$_Error[] = "A képet már benevezték a pálázatra!";
			} else {
				$db->query("insert into competition_entry (competition_id, image_id) VALUES(:competition_id, :image_id) ", array(":competition_id" => $URL[1], ":image_id" => $img));
			}
		}
	}

	if($_POST['q'] == "imageVote") {
		$db->query("insert into competition_vote (competition_id, user_id, image_id) values (:comp_id, :user_id, :image_id)", array(":image_id" => $_POST['inputImageID'], ":comp_id" => $URL['1'], ":user_id" => $_User->getUserId()));
	}
}

$competition = $competition[0];

// A user kepeinek lekerdezese, amit be akar nevezni esetleg
$db->query("select images.id, albums.title as album, images.title as image from albums, images where images.album_id = albums.id AND user_id = :user_id", array(":user_id" => $_User->getUserId()));
$userImages = $db->fetchAll();

// lekerdezem a benevezett kepeket
$db->query("select image_id as ID from competition_entry where competition_id = :competition_id", array(":competition_id" => $URL[1]));

$images = $db->fetchAll();

$voteEnd = false;
//"Ha a vege tegnap volt akkor nem lehet szavazni tobbet"
$ma = new DateTime(date('Y-m-d'));
$vege = new DateTime($competition['END_DATE']);

$diff = $ma->diff($vege);

if($diff->days < 1) {
	$voteEnd = true;
}


?>