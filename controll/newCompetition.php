<?php

//ha kaptunk POST adatokat akkor lekezeljuk
if(isset($_POST['q'])) {
	if ($_POST['inputTitle'] != "" and $_POST['inputText'] != "" and $_POST['inputEnd'] != ""){
		if (($timestamp = strtotime($_POST['inputEnd'])) === false) {
			$_Error[] = "Érvénytelen dátum";
		} else if ($timestamp < time()){
			$_Error[] = "A mai dátumnál korábbi dátumot adott meg lejáratnak!";
		} else {
			$db = new db();
			$db->query("INSERT INTO competitions (user_id, start_date, end_date, title, text) VALUES (:id, CURRENT_TIMESTAMP, TO_TIMESTAMP(:end, 'YYYY-MM-DD'), :title, :text)", array(':id' => $_User->getUserId(), ':end' => $_POST['inputEnd'], ':title' => $_POST['inputTitle'], ':text' => $_POST['inputText']));
			header("location: competitions");
			exit;
		}
	} else {
		$_Error[] = "Minden adatot meg kell adnia!";
	}
}


?>