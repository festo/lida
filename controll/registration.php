<?php

if(isset($_POST['q'])) {

	if($_POST['passwInput'] != $_POST['passwInput2']) {
		$_Error[] = "A két jelszó nem egyezik!";
	} else {
		try{
			if($_User->newUser($_POST['emailInput'], $_POST['passwInput'])) {
				header("location: successRegistration");
			} else {
				$_Error[] = "A művelet közben hiba történt!";
			}
		} catch(NotValidEmailException $e) {
			$_Error[] = "Nem megfelelő e-mail cím!";
		} catch(IncorrectUsernameOrPasswordException $e) {
			$_Error[] = "Hibás felhazsnálónév vagy jelszó!";
		} catch(ExistingEmailException $e) {
			$_Error[] = "Ezzel az e-mail címmel már regisztráltak!";
		}
	}
}
?>