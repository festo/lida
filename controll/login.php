<?php

//ha kaptunk POST adatokat akkor lekezeljuk
if(isset($_POST['q'])) {
	try {
		if($_User->login($_POST['emailInput'],$_POST['passwInput'])) {
			$_SESSION['is_logged'] = true;
			$_SESSION['user_id'] = $_User->getUserId();
			header("Location: dashboard");
			exit;
		} else {
			throw new IncorrectUsernameOrPasswordException;
			
		}	
	} catch(NotValidEmailException $e) {
		$_Error[] = "Nem megfelelő e-mail cím!";
	} catch(IncorrectUsernameOrPasswordException $e) {
		$_Error[] = "Hibás felhasználónév vagy jelszó!";
	}
}


?>