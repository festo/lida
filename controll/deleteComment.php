<?php

// ha nem kapott parametert
if(!isset($URL[1])) {
	header("location: myAlbums");
	exit;	
} else if($URL[1] == '') {
	header("location: ../myAlbums");
	exit;
}

// Ha a user tulajdonaban van a kep akkor toroljuk, ha nincs akkor visszadobjuk.
$db = new db();
$db->query("select * from albums, images, comments where albums.id = images.album_id and images.id = comments.image_id and comments.id = :comm_id and albums.user_id = :user_id", array(":comm_id" => (int)$URL[1], ":user_id" => $_User->getUserId()));
$result=$db->fetchAll();

if($db->numRows() == 1) {
	$db->query("delete from comments where id = :id", array(":id" => (int)$URL[1]));
	// visszaugrunk az adott kephez
	header("location: ../editImage/". $result[0]['IMAGE_ID'] );
	exit;
} else {
	// visszaugrunk az albumokhoz
	header("location: ../myAlbums");
	exit;
}

?>