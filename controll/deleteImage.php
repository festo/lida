<?php

// ha nem kapott parametert
if(!isset($URL[1])) {
	header("location: browse");
	exit;	
} else if($URL[1] == '') {
	header("location: ../browse");
	exit;
}

// Ha a user tulajdonaban van a kep akkor toroljuk, ha nincs akokr atdobjuk a bongeszore
$db = new db();
$db->query("select * from albums, images where albums.id = images.album_id and images.id = :image_id and albums.user_id = :user_id", array(":image_id" => (int)$URL[1], ":user_id" => $_User->getUserId()));
$db->fetchAll();

if($db->numRows() == 1) {
	$db->query("delete from images where id = :id", array(":id" => (int)$URL[1]));
}

// visszaugrunk az albumokhoz
header("location: ../myAlbums");
exit;

?>