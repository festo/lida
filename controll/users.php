<?php


/**
 * Az album elso kepenek az ID-jet adja vissza a boritokephez
 */
function firstImage($albumId) {
	$db = new db();
	$db->query("select id from images where album_id = :album_id AND ROWNUM < 2 order by uploaded_time", array("album_id" => (int)$albumId));
	$result = $db->fetchAll();
	if($db->numRows() != 1) {
		// A show_image.php ha olyan ID-t kap ami nem letezik akkor a placeholder lesz az a kep, amit megjelenit
		return 0;
	} else {
		return $result[0]['ID'];
	}
}


// Ha kapott user id-t parameterul akkor hozzaadjuk a kovetettek listajahoz
if(isset($URL[1]) && $URL[1] == 'follow' && isset($URL[2])) {
	try {
		if(!$_User->follow($URL[2])) {
			$_Error[] = "A művelet közben hiba történt!";
		}
	} catch(NotExistsUserIdException $e) {
		$_Error[] = "Nem létezik a megadott id!";
	}
}	

// Ha a unfollow a parameter akkor pedig toroljuk
if(isset($URL[1]) && $URL[1] == 'unfollow' && isset($URL[2])) {
	try {
		if(!$_User->unfollow($URL[2])) {
			$_Error[] = "A művelet közben hiba történt!";
		}
	} catch(NotExistsUserIdException $e) {
		$_Error[] = "Nem létezik a megadott id!";
	}
}

$db = new db();
$db->query("SELECT id FROM USERS");
$users = $db->fetchAll();

$profile = false;
// ha egy szam a parameter akkor valaki profil oldalat kellene megmutatni
if(isset($URL[1]) && is_int((int)$URL[1]) && (0 != (int)$URL[1])) {
	$profile = true;
	$db->query("Select id from albums where id = :id", array(":id" => (int)$URL[1]));
	$albumIds = $db->fetchAll();

	$userData = new User((int)$URL[1]);
	$userData = $userData->getUserData();

}

?>