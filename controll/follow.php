<?php

// Ha a unfollow a parameter akkor pedig toroljuk
if(isset($URL[1]) && $URL[1] == 'unfollow' && isset($URL[2])) {
	try {
		if(!$_User->unfollow($URL[2])) {
			$_Error[] = "A művelet közben hiba történt!";
		}
	} catch(NotExistsUserIdException $e) {
		$_Error[] = "Nem létezik a megadott id!";
	}
}

$db = new db();
$db->query("SELECT WHOM AS ID FROM FOLLOWS WHERE WHO = :who", array(":who" => $_User->getUserId()));
$users = $db->fetchAll();

?>